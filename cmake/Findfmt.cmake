cmake_minimum_required(VERSION 3.21)

if (NOT fmt_FOUND)

    INCLUDE(FetchContent)

    FetchContent_Declare(fmt GIT_REPOSITORY https://github.com/fmtlib/fmt.git GIT_TAG master)
    FetchContent_MakeAvailable(fmt)

    SET(BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

    set(fmt_FOUND TRUE)
endif()
