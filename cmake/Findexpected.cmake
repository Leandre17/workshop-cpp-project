cmake_minimum_required(VERSION 3.21)

if (NOT expected_FOUND)

    ###############################################################################
    # INSTALL EXPECTED FROM GITHUB REPOSITORY
    ###############################################################################
    INCLUDE(FetchContent)

    FetchContent_Declare(expected GIT_REPOSITORY https://github.com/TartanLlama/expected GIT_TAG v1.0.0 SOURCE_SUBDIR cmake)

    SET(BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
    FetchContent_MakeAvailable(expected)

    ###############################################################################
    # CREATE TARGET AND SET 'INCLUDE_DIRECTORIES' PROPERTY
    ###############################################################################
    add_custom_target(EXPECTED_INCLUDES)
    if (WIN32 OR WIN64)
        set(BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/..)
    else()
        set(BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR})
    endif()
    set_target_properties(EXPECTED_INCLUDES PROPERTIES INCLUDE_DIRECTORIES ${BINARY_DIR}/_deps/expected-src/include/)

    ###############################################################################
    # INITIALIZE 'CMAKE_PROPERTY_LIST' IF IT DOESN'T EXIST
    ###############################################################################
    if(NOT CMAKE_PROPERTY_LIST)
        execute_process(COMMAND cmake --help-property-list OUTPUT_VARIABLE CMAKE_PROPERTY_LIST)

        string(REGEX REPLACE ";" "\\\\;" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")
        string(REGEX REPLACE "\n" ";" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")
    endif()

    ###############################################################################
    # CREATE FUNCTION TO GET INCLUDE PROPERTY OF OUR TARGET
    ###############################################################################
    function(get_expected_include_property target)
        if(NOT TARGET ${target})
            message(STATUS "There is no target named '${target}'")
            return()
        endif()
        get_property(was_set TARGET ${target} PROPERTY "INCLUDE_DIRECTORIES" SET)
        if (NOT was_set)
            message(STATUS "There is no property named 'INCLUDE_DIRECTORIES' in ${target}")
            return()
        endif()
        get_property(value TARGET ${target} PROPERTY "INCLUDE_DIRECTORIES")
        set(value_EXPECTED_INCLUDES ${value} PARENT_SCOPE)
    endfunction()

    ###############################################################################
    # SAY WE FOUND THE LIBRARY
    ###############################################################################
    set(expected_FOUND TRUE)
endif()
