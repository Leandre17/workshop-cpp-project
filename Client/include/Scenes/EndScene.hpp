/**
 * @file EndScene.hpp
 * @author Afilak
 * @brief file for the end scenes when players win
 * @version 0.1
 */

#pragma once

#include "Game/Player.hpp"
#include "GameEngine/ECS/Core/Entity.hpp"
#include "GameEngine/Scene.hpp"

/**
 * @namespace scene
 * @brief scenes related functions and objects
 */
namespace scene {

/**
 * @class LoseScene
 * @brief the end scene class
 */
class LoseScene final : public ge::IScene {
   public:
    LoseScene() noexcept = delete;
    /**
     * @brief end scene object constructor
     *
     * @param score score of the player
     */
    explicit LoseScene(int score) noexcept;
    LoseScene(LoseScene const& other) noexcept = delete;
    LoseScene(LoseScene&& other) noexcept = delete;
    LoseScene& operator=(LoseScene const& other) noexcept = delete;
    LoseScene& operator=(LoseScene&& other) noexcept = delete;
    ~LoseScene() noexcept final;
    /**
     * @brief update the game scene
     *
     * @param elapsedTime
     */
    void update(float elapsedTime) final;

   private:
    /**
     * @var backgroundPath
     * @brief the path to the background sprite
     */
    constexpr static std::string_view backgroundPath = "background/bg-end.png";
    /**
     * @var fontPath
     * @brief the path to the font used in the lose scene
     */
    constexpr static std::string_view fontPath = "arcade.ttf";
    /**
     * @var scoreText
     * @brief the entity that is gonna to display the score of the players
     */
    ge::ecs::core::Entity scoreText;
    /**
     * @var text
     * @brief the entity that is gonna to display end text
     */
    ge::ecs::core::Entity text;
    /**
     * @var hintText
     * @brief the entity that is gonna to display hint for player
     */
    ge::ecs::core::Entity hintText;
    /**
     * @var background
     * @brief the entity that is gonna to display the lose background
     */
    ge::ecs::core::Entity background;
};

} // namespace scene