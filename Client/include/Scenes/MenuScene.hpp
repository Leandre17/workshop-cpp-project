/**
 * @file MenuScene.hpp
 * @author Afilak
 * @brief file for the definition of the Menu scene
 * @version 1
 */

#pragma once

#include "Game/Player.hpp"
#include "GameEngine/ECS/Core/Entity.hpp"
#include "GameEngine/Scene.hpp"

namespace scene {

/**
 * @class MenuScene
 * @brief the Menu scene class
 */
class MenuScene final : public ge::IScene {
   public:
    /**
     * @brief Menu Scene object constructor
     */
    MenuScene();
    MenuScene(MenuScene const& other) noexcept = delete;
    MenuScene(MenuScene&& other) noexcept = delete;
    MenuScene& operator=(MenuScene const& other) noexcept = delete;
    MenuScene& operator=(MenuScene&& other) noexcept = delete;
    ~MenuScene() noexcept final;

    void update(float elapsedTime) final;

   private:
    /**
     * @var backgroundPath
     * @brief the path to the background sprite
     */
    constexpr static std::string_view backgroundPath = "background/bg-start.png";
    /**
     * @var fontPath
     * @brief the path to the font used in the lose scene
     */
    constexpr static std::string_view fontPath = "arcade.ttf";
    /**
     * @var scoreText
     * @brief the entity that is gonna to display how to play of the at the players
     */
    ge::ecs::core::Entity howPlayText;
    /**
     * @var text
     * @brief the entity that is gonna to display end text
     */
    ge::ecs::core::Entity text;
    /**
     * @var hintText
     * @brief the entity that is gonna to display hint for player
     */
    ge::ecs::core::Entity hintText;
    /**
     * @var background
     * @brief the entity that is gonna to display the lose background
     */
    ge::ecs::core::Entity background;
};
} // namespace scene