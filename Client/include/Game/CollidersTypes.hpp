/**
 * @file CollidersTypes.hpp
 * @brief Different colliders types definition
 * @author curs3w4ll
 * @version 1
 */

namespace game {

/**
 * @enum CollidersTypes
 * @brief Different colliders types
 */
enum class CollidersTypes {
    DAMAGEABLE = 1,
    DAMAGER = 2,
    OBSTACLE = 4,
    ENEMY = 8,
    PLAYER = 16,
    WALL = 32,
};

} // namespace game
