/**
 * @file BorderWalls.hpp
 * @brief Map border walls
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "ECS/Core/Coordinator.hpp"

namespace game {

/**
 * @class BorderWalls
 * @brief Map border invisible walls
 */
class BorderWalls {
   public:
    BorderWalls() noexcept = delete;
    /**
     * @brief Construct border walls
     *
     * @param coordinator The ECS coordinator
     */
    explicit BorderWalls(ecs::core::Coordinator& coordinator);
    BorderWalls(BorderWalls const& other) noexcept = delete;
    BorderWalls(BorderWalls&& other) noexcept = default;
    ~BorderWalls() noexcept = default;

    BorderWalls& operator=(BorderWalls const& other) noexcept = delete;
    BorderWalls& operator=(BorderWalls&& other) noexcept = delete;

   private:
    ecs::core::Entity upWall;
    ecs::core::Entity downWall;
    ecs::core::Entity leftWall;
    ecs::core::Entity rightWall;
};

} // namespace game
