#include <exception>
#include <string>

namespace network::error {

class NetworkError : public std::exception {
   public:
    NetworkError() noexcept = delete;
    explicit NetworkError(std::string const& message);
    NetworkError(NetworkError const& other) noexcept = delete;
    NetworkError(NetworkError&& other) noexcept = delete;
    NetworkError& operator=(NetworkError const& other) noexcept = delete;
    NetworkError& operator=(NetworkError&& other) noexcept = delete;
    ~NetworkError() noexcept override = default;

    const char* what() const noexcept final;

   protected:
    std::string message;
};

} // namespace network::error
