/**
 * @file Renderer.hpp
 * @brief Window related objects rendering class
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "ECS/Systems/ImageRenderSystem.hpp"
#include "SFML/Graphics/RenderWindow.hpp"

namespace graphic {

/**
 * @class Renderer
 * @brief Object rendering class
 */
class Renderer {
   public:
    Renderer() noexcept = delete;
    explicit Renderer(ecs::core::Coordinator& coordinator);
    Renderer(Renderer const& other) noexcept = delete;
    Renderer(Renderer&& other) noexcept = default;
    ~Renderer() noexcept = default;

    Renderer& operator=(Renderer const& other) noexcept = delete;
    Renderer& operator=(Renderer&& other) noexcept = delete;

    /**
     * @brief display
     *
     * @pre Before calling this function, you should clear the window you are displaying on if you want
     *
     * @param window The window to display objects on
     */
    void display(sf::RenderWindow& window) const noexcept;

   private:
    /**
     * @var imageRenderSystem
     * @brief Image renderer system
     */
    ecs::system::ImageRenderSystem& imageRenderSystem;
};

} // namespace graphic
