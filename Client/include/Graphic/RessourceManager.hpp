/**
 * @file RessourceManager.hpp
 * @brief Manage graphic ressources for optimization
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "SFML/Audio/SoundBuffer.hpp"
#include "SFML/Graphics/Texture.hpp"

#include <string>
#include <unordered_map>

namespace graphic {

namespace error {

    class TextureError : public std::exception {
       public:
        TextureError() noexcept = delete;
        /**
         * @brief Generate an object to throw when an error occurs in audio
         *
         * @param message A message that describe the error
         */
        explicit TextureError(std::string const& message);
        TextureError(TextureError const& other) = default;
        TextureError(TextureError&& other) = default;
        ~TextureError() override = default;

        TextureError& operator=(TextureError const& other) = default;
        TextureError& operator=(TextureError&& other) = default;

        /**
         * @brief Get a description of the error
         *
         * @return The error message defined in constructor
         */
        const char* what() const noexcept override;

       protected:
        /**
         * @var message
         * @brief Store the message gived in the constructor that describe the error
         */
        std::string message;
    };

} // namespace error

/**
 * @class RessourceManager
 * @brief Manage graphic ressources for optimization
 */
class RessourceManager {
   public:
    RessourceManager(RessourceManager const& other) noexcept = delete;
    RessourceManager(RessourceManager&& other) noexcept = delete;
    ~RessourceManager() noexcept = default;

    RessourceManager& operator=(RessourceManager const& other) noexcept = delete;
    RessourceManager& operator=(RessourceManager&& other) noexcept = delete;

    /**
     * @brief Get a graphic texture
     *
     * @param filepath The path of the file to load the texture from
     *
     * @return A reference to the texture
     *
     * @throw TextureError thrown if the given file is invalid
     */
    static sf::Texture& getTexture(std::string filepath);
    /**
     * @brief Get a sound buffer
     *
     * @param filepath The path of the file to load the sound from
     *
     * @return A reference to the sound buffer
     *
     * @throw SoundError thrown if the given file is invalid
     */
    static sf::SoundBuffer& getSoundBuffer(std::string filepath);

   private:
    RessourceManager() noexcept = default;
    /**
     * @brief Get the ressource manager unique instace
     *
     * @return The unique instance
     */
    static RessourceManager& getInstance() noexcept;

    /**
     * @var textures
     * @brief The list of loaded textures
     */
    std::unordered_map<std::string, sf::Texture> textures;
    /**
     * @var sounds
     * @brief The list of loaded sounds
     */
    std::unordered_map<std::string, sf::SoundBuffer> sounds;
};

} // namespace graphic
