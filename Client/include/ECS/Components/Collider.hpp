/**
 * @file Collider.hpp
 * @brief ECS collision callbacks
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "Utils/Callback.hpp"

#include <vector>

namespace ecs::component {

/**
 * @enum ColliderType
 * @brief Type constitution subtype of colliders
 */
enum class ColliderType {
    DAMAGEABLE = 1,
    DAMAGER = 2,
    OBSTACLE = 4,
};

/**
 * @class Collider
 * @brief Collider managing callbacks
 */
class Collider {
   public:
    Collider() noexcept = delete;
    /**
     * @brief Collider type creator template
     *
     * @tparam Args The different collidertypes constituing the final type
     */
    template <typename... Args>
    /**
     * @brief Create a new collider function
     *
     * @param args The collider types
     */
    explicit Collider(Args... args) noexcept
    {
        ((this->type |= static_cast<int>(args)), ...);
    }
    Collider(Collider const& other) noexcept = delete;
    Collider(Collider&& other) noexcept = default;
    ~Collider() noexcept = default;

    Collider& operator=(Collider const& other) noexcept = delete;
    Collider& operator=(Collider&& other) noexcept = default;

    /**
     * @brief Get the type of the collider
     *
     * @return The collider composition type
     */
    int getType() const noexcept;
    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a new callback for any collision
     *
     * @param f The function to use as callback
     * @param args The arguments to pass to the function
     */
    Collider& addAnyCallback(F&& f, Args&&... args) noexcept
    {
        this->withAny_callbacks.emplace_back(std::forward<F>(f), std::forward<Args>(args)...);

        return *this;
    }
    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a new callback for a collision with damageable objects
     *
     * @param f The function to use as callback
     * @param args The arguments to pass to the function
     */
    Collider& addDamageableCallback(F&& f, Args&&... args) noexcept
    {
        this->withDamageable_callbacks.emplace_back(std::forward<F>(f), std::forward<Args>(args)...);

        return *this;
    }
    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a new callback for a collision with damager objects
     *
     * @param f The function to use as callback
     * @param args The arguments to pass to the function
     */
    Collider& addDamagerCallback(F&& f, Args&&... args) noexcept
    {
        this->withDamager_callbacks.emplace_back(std::forward<F>(f), std::forward<Args>(args)...);

        return *this;
    }
    /**
     * @brief Function pointer template
     *
     * @tparam F The function definition
     * @tparam Args Arguments to pass to the function
     */
    template <typename F, typename... Args>
    /**
     * @brief Add a new callback for a collision with obstacle objects
     *
     * @param f The function to use as callback
     * @param args The arguments to pass to the function
     */
    Collider& addObstacleCallback(F&& f, Args&&... args) noexcept
    {
        this->withObstacle_callbacks.emplace_back(std::forward<F>(f), std::forward<Args>(args)...);

        return *this;
    }
    /**
     * @brief Run associated callbacks for the given in collision object type
     *
     * @param type The type of the object we collided with
     *
     * @throw Anything callbacks could throw
     */
    void runCallbacksFor(int type);

   private:
    /**
     * @var type
     * @brief Collider type of the collider
     */
    int type = 0;
    /**
     * @var withAny_callbacks
     * @brief Callbacks to call when colliding with object of any type
     */
    std::vector<utils::Callback> withAny_callbacks;
    /**
     * @var withDamageable_callbacks
     * @brief Callbacks to call when colliding with object of damageable type
     */
    std::vector<utils::Callback> withDamageable_callbacks;
    /**
     * @var withDamager_callbacks
     * @brief Callbacks to call when colliding with object of damager type
     */
    std::vector<utils::Callback> withDamager_callbacks;
    /**
     * @var withObstacle_callbacks
     * @brief Callbacks to call when colliding with object of obstacle type
     */
    std::vector<utils::Callback> withObstacle_callbacks;
};

} // namespace ecs::component
