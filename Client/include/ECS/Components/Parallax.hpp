/**
 * @file Parallax.hpp
 * @brief Parallax component to set background
 * @author Giabibi
 * @version 1
 */

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string_view>

#pragma once

namespace ecs::component {
/**
 * @var Parallax
 * @brief Parallax is entity component to move entity when it's too far
 */
using Parallax = char;

} // namespace ecs::component
