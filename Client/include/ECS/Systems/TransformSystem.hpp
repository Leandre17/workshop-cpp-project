/**
 * @file TransformSystem.hpp
 * @brief Transformation system definition
 * @author Baptiste-MV
 * @version 1
 */

#include "ECS/Core/ASystem.hpp"
#include "ECS/Core/Coordinator.hpp"

#pragma once

namespace ecs::system {

/**
 * @class TransformSystem
 * @brief The ECS system transformations
 */
class TransformSystem final : public core::ASystem {
   public:
    TransformSystem() noexcept = delete;
    explicit TransformSystem(ecs::core::Coordinator& coordinator) noexcept;
    TransformSystem(TransformSystem const& other) noexcept = delete;
    TransformSystem(TransformSystem&& other) noexcept = default;
    ~TransformSystem() noexcept final = default;

    TransformSystem& operator=(TransformSystem const& other) noexcept = delete;
    TransformSystem& operator=(TransformSystem&& other) noexcept = delete;

    /**
     * @brief update the transformations
     *
     * @param elapsedTime The time passed in game
     */
    void update(float elapsedTime) const;
};

} // namespace ecs::system