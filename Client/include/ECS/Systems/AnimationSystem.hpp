#include "ECS/Core/ASystem.hpp"
#include "ECS/Core/Coordinator.hpp"

#pragma once

namespace ecs::system {

class AnimationSystem final : public core::ASystem {
   public:
    AnimationSystem() noexcept = delete;
    explicit AnimationSystem(ecs::core::Coordinator& coordinator) noexcept;
    AnimationSystem(AnimationSystem const& other) noexcept = delete;
    AnimationSystem(AnimationSystem&& other) noexcept = default;
    ~AnimationSystem() noexcept final = default;

    AnimationSystem& operator=(AnimationSystem const& other) noexcept = delete;
    AnimationSystem& operator=(AnimationSystem&& other) noexcept = delete;

    void update(float elaspedTime) const;
};

} // namespace ecs::system
