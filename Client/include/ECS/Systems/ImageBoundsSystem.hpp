/**
 * @file ImageBoundsSystem.hpp
 * @brief Bounds of image entity system definition
 * @author Baptiste-MV
 * @version 1
 */

#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Core/ASystem.hpp"
#include "ECS/Core/Coordinator.hpp"
#include "ECS/Core/Entity.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <string_view>

#pragma once

namespace ecs::system {

/**
 * @class ImageBoundsSystem
 * @brief The ECS system bounds for images
 */
class ImageBoundsSystem final : public core::ASystem {
   public:
    ImageBoundsSystem() noexcept = delete;
    explicit ImageBoundsSystem(ecs::core::Coordinator& coordinator) noexcept;
    ImageBoundsSystem(ImageBoundsSystem const& other) noexcept = delete;
    ImageBoundsSystem(ImageBoundsSystem&& other) noexcept = default;
    ~ImageBoundsSystem() noexcept final = default;

    ImageBoundsSystem& operator=(ImageBoundsSystem const& other) noexcept = delete;
    ImageBoundsSystem& operator=(ImageBoundsSystem&& other) noexcept = delete;

    /**
     * @brief update the bounds of the images
     *
     * @param coordinator The ECS coordinator
     */
    void update() const;
};

} // namespace ecs::system