/**
 * @file Utils.hpp
 * @brief Utilitary functions that can do anything
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "ECS/Core/Coordinator.hpp"
#include "SFML/System/Vector2.hpp"

/**
 * @namespace utils
 * @brief Utilitary functions and classes
 */
namespace utils {

/**
 * @brief Register a set of components inside the given coordinator
 *
 * @param coordinator The ECS coordinator to register components in
 */
void registerComponents(ecs::core::Coordinator& coordinator);
/**
 * @brief Check if usage have been asked in command line arguments
 *
 * @param ac The number of command line arguments
 * @param av The command line arguments
 *
 * @return `true` if a usage have been detected, so need to quit
 */
bool checkUsage(int ac, char const* av[]) noexcept;
/**
 * @brief Display the actual game FPS
 *
 * @param dt The time elapsed since the last call
 */
void displayFPS(float dt) noexcept;
/*
 * @brief Get the rotation angle of the given vector according to x axis
 *
 * @return The rotation angle in degrees
 *
 * @param vector The vector to get angle for
 */
float getRotationAngleFromX(const sf::Vector2f& vector) noexcept;

} // namespace utils
