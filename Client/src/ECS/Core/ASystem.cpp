#include "ECS/Core/ASystem.hpp"

ecs::core::ASystem::ASystem(Coordinator& coordinator) noexcept :
    coordinator(coordinator)
{
}

void ecs::core::ASystem::addEntity(Entity& entity) noexcept
{
    if (std::find(this->entities.begin(), this->entities.end(), std::size_t{entity}) == this->entities.end()) {
        this->entities.emplace_back(std::size_t{entity});
    }
}

void ecs::core::ASystem::removeEntity(Entity& entity) noexcept
{
    auto found = std::find(this->entities.begin(), this->entities.end(), std::size_t{entity});
    if (found != this->entities.end()) {
        this->entities.erase(found);
    }
}
