#include "ECS/Core/EntitiesManager.hpp"

#include "ECS/ECSErrors.hpp"
#include "fmt/core.h"

#include <algorithm>
#include <iostream>

ecs::core::Entity ecs::core::EntitiesManager::createEntity() noexcept
{
    if (not this->availableEntities.empty()) {
        auto got = this->availableEntities.back();
        this->availableEntities.pop_back();
        return Entity{got};
    }
    if (not this->used) {
        this->used = true;
    } else {
        this->maxExistingEntity++;
    }
    if (this->entitiesSignatures.size() != this->maxExistingEntity + 1) {
        this->entitiesSignatures.resize(this->maxExistingEntity + 1);
    }
    return Entity{this->maxExistingEntity};
}

void ecs::core::EntitiesManager::destroyEntity(Entity& entity) noexcept
{
    if (std::find(this->availableEntities.begin(), this->availableEntities.end(), entity) != this->availableEntities.end() or entity > this->maxExistingEntity) {
        std::cout << fmt::format("WARNING: Deleting the same entity twice ({})", entity) << std::endl;
    } else {
        this->entitiesSignatures[entity].clear();
        this->availableEntities.emplace_back(entity);
    }
}

void ecs::core::EntitiesManager::setSignature(Entity& entity, ComponentSignature signature)
{
    if (std::find(this->availableEntities.begin(), this->availableEntities.end(), entity) != this->availableEntities.end() or entity > this->maxExistingEntity) {
        throw ecs::error::ECSError("Setting signature of non existing or destroyed entity");
    }
    this->entitiesSignatures[entity] = std::forward<ComponentSignature>(signature);
}

void ecs::core::EntitiesManager::sign(Entity& entity, Bit componentBit, bool value)
{
    if (std::find(this->availableEntities.begin(), this->availableEntities.end(), entity) != this->availableEntities.end() or entity > this->maxExistingEntity) {
        throw ecs::error::ECSError("Changing signature of non existing or destroyed entity");
    }
    this->entitiesSignatures[entity].set(componentBit, value);
}

ecs::core::ComponentSignature ecs::core::EntitiesManager::getSignature(Entity& entity) const
{
    if (std::find(this->availableEntities.begin(), this->availableEntities.end(), entity) != this->availableEntities.end() or entity > this->maxExistingEntity) {
        throw ecs::error::ECSError("Getting signature of non existing or destroyed entity");
    }
    return this->entitiesSignatures.at(entity);
}
