#include "ECS/Core/Bitset.hpp"

#include <bitset>
#include <utility>

void ecs::core::Bitset::clear() noexcept
{
    this->data.clear();
}

bool ecs::core::Bitset::operator!=(const Bitset& other) const
{
    const auto& longer = this->data.size() > other.data.size() ? this->data : other.data;
    const auto& shorter = this->data.size() > other.data.size() ? other.data : this->data;

    for (int i = shorter.size(); i < longer.size(); i++) {
        if (longer[i] != 0) {
            return true;
        }
    }

    for (int i = 0; i < shorter.size(); i++) {
        if ((this->data[i] ^ other.data[i]) != 0) {
            return true;
        }
    }

    return false;
}

bool ecs::core::Bitset::operator==(const Bitset& other) const
{
    return !(*this != other);
}

void ecs::core::Bitset::set(std::size_t i, bool value) noexcept
{
    auto const byte = i / 8;
    auto const bit = i % 8;

    if (byte >= this->data.size()) {
        this->data.resize(byte + 1);
    }
    if (value) {
        this->data[byte] |= (1 << bit);
    } else {
        this->data[byte] &= ~(1 << bit);
    }
}

bool ecs::core::Bitset::get(std::size_t i) const noexcept
{
    auto const byte = i / 8;
    auto const bit = i % 8;

    if (byte >= this->data.size()) {
        return false;
    }

    return (this->data[byte] & (1 << bit)) > 0;
}

bool ecs::core::Bitset::contains(const Bitset& other) const noexcept
{
    if (other.data.size() > this->data.size()) {
        for (int i = this->data.size(); i < other.data.size(); i++) {
            if (other.data[i] != 0) {
                return false;
            }
        }
    }

    const auto shorterSize = this->data.size() > other.data.size() ? other.data.size() : this->data.size();

    for (int i = 0; i < shorterSize; i++) {
        if ((this->data[i] & other.data[i]) != other.data[i]) {
            return false;
        }
    }
    return true;
}

std::vector<ecs::core::Byte> ecs::core::Bitset::extractData() const noexcept
{
    return this->data;
}

void ecs::core::Bitset::importData(std::vector<Byte> binaryData) noexcept
{
    this->data.clear();
    this->data = std::move(binaryData);
}

// NOLINTNEXTLINE
std::ostream& operator<<(std::ostream& out, ecs::core::Bitset bitset)
{
    auto const data = bitset.extractData();
    for (const ecs::core::Byte& byte : data) {
        std::bitset<8> bs(byte);
        out << bs.to_string() << " " << std::endl;
    }

    return out;
}
