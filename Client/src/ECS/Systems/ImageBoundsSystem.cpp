#include "ECS/Systems/ImageBoundsSystem.hpp"

ecs::system::ImageBoundsSystem::ImageBoundsSystem(ecs::core::Coordinator& coordinator) noexcept :
    ecs::core::ASystem(coordinator) { }

void ecs::system::ImageBoundsSystem::update() const
{
    for (auto const& entity : this->entities) {
        auto& sprite = this->coordinator.getComponent<ecs::component::RenderableImage>(entity);
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(entity);

        bounds.bounds = sprite.getBounds();
    }
}