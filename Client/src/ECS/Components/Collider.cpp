#include "ECS/Components/Collider.hpp"

int ecs::component::Collider::getType() const noexcept
{
    return this->type;
}

static void runCallbacks(std::vector<utils::Callback>& callbacks)
{
    for (const auto& callback : callbacks) {
        callback();
    }
}

void ecs::component::Collider::runCallbacksFor(int type)
{
    runCallbacks(this->withAny_callbacks);

    if ((type & static_cast<int>(ColliderType::DAMAGEABLE)) > 0) {
        runCallbacks(this->withDamageable_callbacks);
    }
    if ((type & static_cast<int>(ColliderType::DAMAGER)) > 0) {
        runCallbacks(this->withDamager_callbacks);
    }
    if ((type & static_cast<int>(ColliderType::OBSTACLE)) > 0) {
        runCallbacks(this->withObstacle_callbacks);
    }
}
