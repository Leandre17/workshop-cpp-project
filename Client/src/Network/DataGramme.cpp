/*
** EPITECH PROJECT, 2022
** Client
** File description:
** DataGramme
*/

#include "Network/DataGramme.hpp"

#include "GameEngine/Utils/Bitset.hpp"

#include <iostream>

void network::DataGramme::clearBinaryData() noexcept
{
    this->binaryData.clear();
}

void network::DataGramme::clearAll() noexcept
{
    this->binaryData.clear();
    this->importantData.clear();
    this->normalData.clear();
    this->lowData.clear();
}

std::vector<std::vector<std::vector<Byte>>> network::DataGramme::getBinaryData() noexcept
{
    this->binaryData.clear();

    this->binaryData.push_back(this->importantData);
    this->binaryData.push_back(this->normalData);
    this->binaryData.push_back(this->lowData);
    return this->binaryData;
}

void network::DataGramme::addImportantData(const std::vector<Byte>& data) noexcept
{
    this->importantData.push_back(data);
}

std::vector<std::vector<Byte>> network::DataGramme::getImportantData() noexcept
{
    return this->importantData;
}

void network::DataGramme::addNormalData(const std::vector<Byte>& data) noexcept
{
    this->normalData.push_back(data);
}

std::vector<std::vector<Byte>> network::DataGramme::getNormalData() noexcept
{
    return this->normalData;
}

void network::DataGramme::addLowData(const std::vector<Byte>& data) noexcept
{
    this->lowData.push_back(data);
}

std::vector<std::vector<Byte>> network::DataGramme::getLowData() noexcept
{
    return this->lowData;
}
