#include "Network/ClientNetwork.hpp"

#include "Network/NetworkSerialiser.hpp"
#include "Network/Offset.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "SFML/Network/Socket.hpp"

#include <algorithm>
#include <mutex>
#include <optional>
#include <thread>
#include <vector>

network::ClientNetwork::ClientNetwork(sf::IpAddress servIp, unsigned short& serverPort) :
    serverIp(servIp), serverPort(serverPort)
{

    if (this->clientSocket.bind(this->clientSocket.getLocalPort()) != sf::Socket::Status::Done) {
        throw error::NetworkError("Can't bind the socket port for send maybe already use.");
    }

    this->players.resize(4);
}

void network::ClientNetwork::updatePlayer(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage) noexcept
{

    std::unique_lock<std::mutex> const ltx(this->playerMutex);

    std::vector<Byte> playerData{};

    for (auto& player : this->players) {

        if (player.id == id) {
            player.pos = pos;
            player.life = life;
            player.damage = damage;
            playerData = this->networkSerialiser.convertPlayerToBinary(player);
        }
    }
    std::unique_lock<std::mutex> const dataltx(this->dataGrammeMutex);
    this->dataGramme.insert(this->dataGramme.end(), playerData.begin(), playerData.end());
}

void network::ClientNetwork::updateEnemy(unsigned short id, sf::Vector2f pos, unsigned short life, unsigned short damage) noexcept
{

    std::unique_lock<std::mutex> const ltx(this->enemyMutex);

    std::vector<Byte> enemyData{};

    for (auto& enemy : this->enemys) {

        if (enemy.id == id) {
            enemy.pos = pos;
            enemy.life = life;
            enemy.damage = damage;
            enemyData = this->networkSerialiser.convertEnemyToBinary(enemy);
        }
    }
    std::unique_lock<std::mutex> const dataltx(this->dataGrammeMutex);
    this->dataGramme.insert(this->dataGramme.end(), enemyData.begin(), enemyData.end());
    this->removeEnemy(id);
}

void network::ClientNetwork::updateProjectile(unsigned short id, sf::Vector2f pos, unsigned short damage, unsigned short ownerId) noexcept
{
    std::unique_lock<std::mutex> const ltx(this->projectMutex);
    std::unique_lock<std::mutex> const dataltx(this->dataGrammeMutex);
    std::vector<Byte> projectileData{};

    for (auto& projectile : this->projectiles) {

        if (projectile.id == id) {
            projectile.pos = pos;
            projectile.damage = damage;
            projectile.ownerId = ownerId;
            projectileData = this->networkSerialiser.convertProjectileToBinary(projectile);
            break;
        }
    }
    this->dataGramme.insert(this->dataGramme.end(), projectileData.begin(), projectileData.end());
}

void network::ClientNetwork::createPlayer(playerInfo& player) noexcept
{
    {

        std::unique_lock<std::mutex> ltx(this->playerMutex);
        this->players.push_back(player);
    }

    {
        std::vector<Byte> playerData = this->networkSerialiser.convertPlayerToBinary(player);
        std::unique_lock<std::mutex> ltx(this->dataGrammeMutex);
        this->dataGramme.insert(this->dataGramme.end(), playerData.begin(), playerData.end());
    }
}

void network::ClientNetwork::createEnemy(enemyInfo& enemy) noexcept
{
    {
        std::unique_lock<std::mutex> ltx(this->enemyMutex);
        this->enemys.push_back(enemy);
    }

    {
        std::vector<Byte> enemyData = this->networkSerialiser.convertEnemyToBinary(enemy);
        std::unique_lock<std::mutex> ltx(this->enemyMutex);
        this->dataGramme.insert(this->dataGramme.end(), enemyData.begin(), enemyData.end());
    }
}

void network::ClientNetwork::createProjectile(projectile& projec) noexcept
{
    {
        std::unique_lock<std::mutex> ltx(this->projectMutex);
        this->projectiles.push_back(projec);
    }

    {
        std::unique_lock<std::mutex> ltx(this->projectMutex);
        std::vector<Byte> projectileData = this->networkSerialiser.convertProjectileToBinary(projec);
        this->dataGramme.insert(this->dataGramme.end(), projectileData.begin(), projectileData.end());
    }

    myProjectilesIds.push_back(projec.id);
}

void network::ClientNetwork::deleteEnemy(unsigned short id) noexcept
{
    std::unique_lock<std::mutex> ltx(this->enemyMutex);

    for (auto i = this->enemys.begin(); i != this->enemys.end(); i++) {
        if (id == i->id) {
            this->enemys.erase(i);
            return;
        }
    }
}

void network::ClientNetwork::deleteProjectile(unsigned short id) noexcept
{
    std::unique_lock<std::mutex> ltx(this->projectMutex);

    for (auto i = this->projectiles.begin(); i != this->projectiles.end(); i++) {
        if (id == i->id) {
            this->projectiles.erase(i);
            return;
        }
    }
}

void network::ClientNetwork::receivePlayerId() noexcept
{
    unsigned short id = 0;
    std::size_t receivedSize = 0;
    std::optional<sf::IpAddress> receiveIp;
    unsigned short serverPort = 0;

    static_cast<void>(this->clientSocket.send(&id, sizeof(unsigned short), this->serverIp, this->serverPort));
    static_cast<void>(this->clientSocket.receive(&id, sizeof(unsigned short), receivedSize, receiveIp, serverPort));
    this->playerID = id;
}

void network::ClientNetwork::sendToServer() noexcept
{
    this->sendClock.saveTimePoint();

    while (this->running) {

        if (this->sendClock.getElapsedTime() >= 1000.00 / 64) {
            std::unique_lock<std::mutex> ltx(this->dataGrammeMutex);

            if (!this->dataGramme.empty() && this->clientSocket.send(this->dataGramme.data(), this->dataGramme.size(), this->serverIp, this->serverPort) != sf::Socket::Status::Done) {
                continue;
            }
            this->dataGramme.clear();
            this->sendClock.saveTimePoint();
        }
    }
}

void network::ClientNetwork::receiveData() noexcept
{

    std::vector<Byte> rowData;
    rowData.resize(1500);
    std::size_t receivedSize = 0;
    std::optional<sf::IpAddress> receivedIp{};
    unsigned short receivedPort = 0;
    bool isPresent = false;

    while (this->running) {

        if (this->clientSocket.receive(rowData.data(), 1500, receivedSize, receivedIp, receivedPort) != sf::Socket::Status::Done) {
            continue;
        }

        while (!rowData.empty()) {
            if (rowData.size() > receivedSize) {
                rowData.resize(receivedSize);
            }
            auto offset = this->networkSerialiser.getOffset(rowData);

            if (offset != 1 && offset != 2 && offset != 3) {
                break;
            }

            switch (offset) {
            case PLAYER: {
                struct playerInfo player = this->networkSerialiser.convertBinaryToPlayer(rowData);
                std::unique_lock<std::mutex> ltx(this->playerMutex);

                for (auto& p : this->players) {
                    if (p.id == player.id) {
                        p.damage = player.damage;
                        p.life = player.life;
                        p.pos = player.pos;
                        isPresent = true;
                        break;
                    }
                }
                if (!isPresent) {
                    this->players.push_back(player);
                }
                isPresent = false;
                break;
            }
            case ENEMY: {
                struct enemyInfo enemy = this->networkSerialiser.convertBinaryToEnemy(rowData);
                std::unique_lock<std::mutex> ltx(this->enemyMutex);

                for (auto& e : this->enemys) {

                    if (e.id == enemy.id) {
                        e.damage = enemy.damage;
                        e.enemyTypeID = enemy.enemyTypeID;
                        e.life = enemy.life;
                        e.pos = enemy.pos;
                        isPresent = true;
                        break;
                    }
                }

                if (!isPresent) {
                    this->enemys.push_back(enemy);
                }
                isPresent = false;
                break;
            }
            case PROJECTILE: {
                struct projectile proj = this->networkSerialiser.convertBinaryToProjectile(rowData);
                std::unique_lock<std::mutex> ltx(this->projectMutex);

                for (auto& p : this->projectiles) {

                    if (p.id == proj.id) {
                        p.damage = proj.damage;
                        p.projectileTypeID = proj.projectileTypeID;
                        p.pos = proj.pos;
                        isPresent = true;
                    }
                }
                if (!isPresent) {
                    this->projectiles.push_back(proj);
                }
                isPresent = false;
                break;
            }
            }
        }
        rowData.clear();
        rowData.resize(1500);
    }
}

void network::ClientNetwork::startNetwork()
{
    this->receivePlayerId();
    this->sendThread = std::jthread(&ClientNetwork::sendToServer, this);
    this->receiveThread = std::jthread(&ClientNetwork::receiveData, this);
}

const unsigned short network::ClientNetwork::getPlayerId() const noexcept
{
    return this->playerID;
}

const std::vector<playerInfo>& network::ClientNetwork::getPlayersInfo() const noexcept
{
    return this->players;
}

const std::vector<enemyInfo>& network::ClientNetwork::getEnemiesInfo() const noexcept
{
    return this->enemys;
}

const std::vector<projectile>& network::ClientNetwork::getProjectilesInfo() const noexcept
{
    return this->projectiles;
}

void network::ClientNetwork::removeEnemy(const unsigned short id) noexcept
{
    for (size_t iterator = 0; iterator < this->enemys.size(); ++iterator) {
        if (this->enemys[iterator].id == id) {
            this->enemys.erase(this->enemys.begin() + iterator);
        }
    }
}

const bool network::ClientNetwork::isMyProjectile(const unsigned short id) noexcept
{
    auto res = std::find(this->myProjectilesIds.begin(), this->myProjectilesIds.end(), id);

    return (res != this->myProjectilesIds.end());
}

void network::ClientNetwork::removeOfMyProjectile(const unsigned short id) noexcept
{
    auto res = std::find(this->myProjectilesIds.begin(), this->myProjectilesIds.end(), id);
    if (res != this->myProjectilesIds.end()) {
        this->myProjectilesIds.erase(res);
    }
}
void network::ClientNetwork::setRunning(bool value) noexcept
{
    this->running = value;
}
