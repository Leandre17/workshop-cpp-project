#include "Audio/SoundPlayer.hpp"

#include "Audio/AudioError.hpp"
#include "Graphic/RessourceManager.hpp"
#include "fmt/core.h"

audio::Sound::Sound(const std::string& filePath, float volume)
{
    auto& buffer = graphic::RessourceManager::getSoundBuffer(filePath);

    this->sound.setBuffer(buffer);
    this->sound.setVolume(volume);
    this->sound.play();
}

void audio::Sound::setVolume(float volume) noexcept
{
    this->sound.setVolume(volume);
}

bool audio::Sound::isEnded() const noexcept
{
    return (this->sound.getStatus() == sf::Sound::Stopped);
}

audio::SoundPlayer& audio::SoundPlayer::getInstance() noexcept
{
    static SoundPlayer soundPlayer;

    return soundPlayer;
}

void audio::SoundPlayer::deleteEndedSounds() noexcept
{
    auto& instance = SoundPlayer::getInstance();

    for (auto it = instance.sounds.begin(); it != instance.sounds.end(); it++) {
        if (it->isEnded()) {
            instance.sounds.erase(it);
            return SoundPlayer::deleteEndedSounds();
        }
    }
}

void audio::SoundPlayer::playSound(const std::string& filePath)
{
    SoundPlayer::deleteEndedSounds();

    auto& instance = SoundPlayer::getInstance();

    float volume = instance.volume;
    if (instance.specificVolumes.contains(filePath)) {
        volume = instance.specificVolumes.at(filePath);
    }

    instance.sounds.emplace_back(filePath, volume);
}

void audio::SoundPlayer::setSoundVolume(std::string filePath, float volume) noexcept
{
    auto& instance = SoundPlayer::getInstance();

    if (instance.specificVolumes.contains(filePath)) {
        instance.specificVolumes.at(filePath) = volume;
    } else {
        instance.specificVolumes.insert({std::move(filePath), volume});
    }
}

void audio::SoundPlayer::setSoundsVolume(float volume) noexcept
{
    auto& instance = SoundPlayer::getInstance();

    instance.volume = volume;
    for (auto& sound : instance.sounds) {
        sound.setVolume(volume);
    }
}
