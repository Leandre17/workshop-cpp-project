#include "Game/Bullet.hpp"

#include "Audio/SoundPlayer.hpp"
#include "ECS/Components/Collider.hpp"
#include "Network/ClientNetwork.hpp"
#include "Utils/FilePaths.hpp"
#include "Utils/Utils.hpp"
#include "fmt/core.h"

#include <iostream>

game::BulletManager::BulletManager(network::ClientNetwork& client) noexcept :
    client(client)
{
}

void game::BulletManager::clearBullets() noexcept
{
    this->listBullets.clear();
}

void game::BulletManager::updateBullets(float elapsedTime, const std::unordered_map<unsigned short, size_t>& projectileEntityIds) noexcept
{
    for (auto it = this->listBullets.begin(); it != this->listBullets.end();) {
        if (not it->second->isAlive()) {
            for (auto it2 = projectileEntityIds.begin(); it2 != projectileEntityIds.end(); ++it2) {
                if (it2->second == it->second->getEntityId()) {
                    this->client.deleteProjectile(it2->first);
                }
            }
            it = this->listBullets.erase(it);
        } else {
            it->second->move(elapsedTime);
            it++;
        }
    }
}

void game::BulletManager::createBullet(ecs::core::Coordinator& coordinator, BulletType type, sf::Vector2f position, sf::Vector2i direction) noexcept
{
    switch (type) {
    case BulletType::ELECTRIC:
        this->listBullets.emplace(this->lastBulletId, std::make_unique<ElectricBullet>(coordinator, position, sf::Vector2f{static_cast<float>(direction.x), static_cast<float>(direction.y)}));
        break;
    case BulletType::BALL:
        this->listBullets.emplace(this->lastBulletId, std::make_unique<BallBullet>(coordinator, position, sf::Vector2f{static_cast<float>(direction.x), static_cast<float>(direction.y)}));
        break;
    }
    this->lastBulletId++;
}

const size_t game::BulletManager::getLastBullet() const noexcept
{
    auto& bullet = this->listBullets.at(this->lastBulletId - 1);

    if (bullet) {
        return bullet->getEntityId();
    }
    return 0;
}

game::ABullet::ABullet(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f direction, float speed, float scale) :
    coordinator(coordinator), entity(coordinator.createEntity())
{
    sf::Vector2f movement = {direction.x * speed, direction.y * speed};

    coordinator.setComponent<ecs::component::Attributes>(this->entity, {.position = position, .scale = {scale, scale}, .angle = utils::getRotationAngleFromX(direction)});
    coordinator.setComponent<ecs::component::Transform>(this->entity, ecs::component::Transform{.movement = movement});
}

game::ABullet::ABullet(ABullet&& other) noexcept :
    coordinator(other.coordinator), entity(std::move(other.entity)), valid(other.valid)
{
    other.valid = false;
}

game::ABullet::~ABullet() noexcept
{
    if (this->valid) {
        this->coordinator.destroyEntity(this->entity);
    }
}

bool game::ABullet::isAlive() const noexcept
{
    return (this->lifePoints > 0);
}

void game::ABullet::hit() noexcept
{
    this->lifePoints--;
}

void game::ABullet::destroy() noexcept
{
    auto attributes = this->coordinator.getComponent<ecs::component::Attributes>(this->entity);

    this->lifePoints = 0;
}

const size_t game::ABullet::getEntityId() const noexcept
{
    return this->entity;
}

game::ElectricBullet::ElectricBullet(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f direction) :
    ABullet(coordinator, position, direction, ElectricBullet::speed, ElectricBullet::scale)
{
    auto& attr = coordinator.getComponent<ecs::component::Attributes>(this->entity);

    auto bounds = coordinator.setComponent<ecs::component::RenderableImage>(this->entity, ElectricBullet::spritePath, ElectricBullet::spriteArea).setScale(attr.scale).setRotation(attr.angle).getBounds();
    if (direction.x > 0) {
        attr.position.x += bounds.width / 2;
    } else if (direction.x < 0) {
        attr.position.x -= bounds.width / 2;
    }
    if (direction.y > 0) {
        attr.position.y += bounds.height / 2;
    } else if (direction.y < 0) {
        attr.position.y -= bounds.height / 2;
    }

    coordinator.setComponent<ecs::component::Animation>(this->entity, ElectricBullet::animationFramesNumber, ElectricBullet::spriteArea, ElectricBullet::animationFrameDuration).start();
    coordinator.setComponent<ecs::component::Bounds>(this->entity);
    coordinator.setComponent<ecs::component::Collider>(this->entity, ecs::component::ColliderType::DAMAGER).addAnyCallback(&ABullet::hit, &(*this)).addObstacleCallback(&ABullet::destroy, &(*this));

    audio::SoundPlayer::playSound(std::string(ElectricBullet::soundPath));
}

void game::ElectricBullet::move(float /*elapsedTime*/) noexcept
{
}

game::BallBullet::BallBullet(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f direction) :
    ABullet(coordinator, position, direction, BallBullet::speed, BallBullet::scale)
{
    auto& attr = coordinator.getComponent<ecs::component::Attributes>(this->entity);

    auto bounds = coordinator.setComponent<ecs::component::RenderableImage>(this->entity, BallBullet::spritePath, BallBullet::spriteArea).setScale(attr.scale).setRotation(attr.angle).getBounds();
    if (direction.x > 0) {
        attr.position.x += bounds.width / 2;
    } else if (direction.x < 0) {
        attr.position.x -= bounds.width / 2;
    }
    if (direction.y > 0) {
        attr.position.y += bounds.height / 2;
    } else if (direction.y < 0) {
        attr.position.y -= bounds.height / 2;
    }
    coordinator.setComponent<ecs::component::Animation>(this->entity, BallBullet::animationFramesNumber, BallBullet::spriteArea, BallBullet::animationFrameDuration).looping().start();
    coordinator.setComponent<ecs::component::Bounds>(this->entity);
    coordinator.setComponent<ecs::component::Collider>(this->entity, ecs::component::ColliderType::DAMAGER).addAnyCallback(&ABullet::hit, &(*this)).addObstacleCallback(&ABullet::destroy, &(*this));

    audio::SoundPlayer::playSound(std::string(BallBullet::soundPath));
}

void game::BallBullet::move(float /*elapsedTime*/) noexcept
{
}
