#include "Audio/AudioError.hpp"
#include "Audio/Music.hpp"
#include "Audio/SoundPlayer.hpp"
#include "ECS/Components/Animation.hpp"
#include "ECS/Components/Area.hpp"
#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Collider.hpp"
#include "ECS/Components/Parallax.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Components/Transform.hpp"
#include "ECS/Core/Coordinator.hpp"
#include "ECS/Systems/AnimationSystem.hpp"
#include "ECS/Systems/AreaBoundsSystem.hpp"
#include "ECS/Systems/BoundsPreviewSystem.hpp"
#include "ECS/Systems/CollisionSystem.hpp"
#include "ECS/Systems/ImageBoundsSystem.hpp"
#include "ECS/Systems/ParallaxSystem.hpp"
#include "ECS/Systems/TransformSystem.hpp"
#include "Game/BorderWalls.hpp"
#include "Game/Bullet.hpp"
#include "Game/Enemy.hpp"
#include "Game/Explosion.hpp"
#include "Game/Parallax.hpp"
#include "Game/Player.hpp"
#include "Game/UpdateDataFromNetwork.hpp"
#include "Graphic/RessourceManager.hpp"
#include "Graphic/Window.hpp"
#include "Network/ClientNetwork.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "Utils/Randomizer.hpp"
#include "Utils/Utils.hpp"

#include <iostream>

sf::IpAddress converIp(std::string& str, char delim)
{
    size_t start = 0;
    size_t end = 0;
    std::vector<std::string> out;
    while ((start = str.find_first_not_of(delim, end)) != std::string::npos) {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }

    return sf::IpAddress(stoi(out[0]), stoi(out[1]), stoi(out[2]), stoi(out[3]));
}

int main(int ac, char const* av[]) noexcept
{
    if (utils::checkUsage(ac, av)) {
        return 0;
    }

    if (ac == 1) {
        std::cerr << "You need to give the ip server" << std::endl;
        return 84;
    }

    ecs::core::Coordinator coordinator;

    try {
        utils::registerComponents(coordinator);
        utils::Randomizer::resetSeed();

        auto& transformSystem = coordinator.registerSystem<ecs::system::TransformSystem, ecs::component::Attributes, ecs::component::Transform>();
        auto& animationSystem = coordinator.registerSystem<ecs::system::AnimationSystem, ecs::component::Animation, ecs::component::RenderableImage>();
        auto& imageBoundsSystem = coordinator.registerSystem<ecs::system::ImageBoundsSystem, ecs::component::RenderableImage, ecs::component::Bounds>();
        auto& areaBoundsSystem = coordinator.registerSystem<ecs::system::AreaBoundsSystem, ecs::component::Area, ecs::component::Attributes, ecs::component::Bounds>();
        auto& boundsPreviewSystem = coordinator.registerSystem<ecs::system::BoundsPreviewSystem, ecs::component::Transform, ecs::component::Bounds>();
        auto& collisionSystem = coordinator.registerSystem<ecs::system::CollisionSystem, ecs::component::Bounds, ecs::component::Collider>();
        auto& parallaxSystem = coordinator.registerSystem<ecs::system::ParallaxSystem, ecs::component::RenderableImage, ecs::component::Attributes, ecs::component::Parallax>();

        graphic::Window window(coordinator, "R-Type");
        window.keyRepeat(false);

        std::vector<std::string> filepaths = {
            "parallax/background_1.png",
            "parallax/background_2.png",
            "parallax/background_3.png",
            "parallax/background_4.png"};

        std::string ip(av[1]);
        sf::IpAddress ipServer = converIp(ip, '.');
        unsigned short port = 55555;
        network::ClientNetwork client(ipServer, port);
        client.startNetwork();

        game::BulletManager bulletManager(client);
        game::ExplosionManager explosionManager;
        game::EnemyManager enemyManager(explosionManager, bulletManager);

        game::UpdateDataFromNetwork manager(coordinator, client, bulletManager, enemyManager);

        game::ParallaxManager parallaxManager(coordinator, filepaths, 100, 500);
        game::BorderWalls borderWalls(coordinator);

        game::Player player(coordinator, bulletManager, window, {50, 540}, client);
        // game::EnemyCreator enemyCreator(coordinator, enemyManager);

        manager.setPlayerEntityId(player.getEntity());

        // audio::Music theme1("mainTheme.wav");
        // theme1.play();

        bool paused = false;
        sf::Clock pause;
        sf::Clock clock;
        float dt = 0;
        while (window.isOpen()) {
            dt = clock.getElapsedTime().asMilliseconds();
            // dt /= 5;
            clock.restart();

            if (not paused) {
                bulletManager.updateBullets(dt, manager.getProjectileEntityIds());
                enemyManager.updateEnemies(dt);

                window.treatEvents();
                window.render();

                animationSystem.update(dt);
                imageBoundsSystem.update();
                areaBoundsSystem.update();
                boundsPreviewSystem.update(dt);
                collisionSystem.update();
                transformSystem.update(dt);
                parallaxSystem.update();
                manager.update();
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) and pause.getElapsedTime().asSeconds() > 0.5) {
                pause.restart();
                paused = !paused;
            }
        }
    } catch (ecs::error::ComponentError const& e) {
        std::cerr << "An error occured in a component of the game engine." << std::endl;
        std::cerr << "Component '" << e.which() << "' throwed:" << std::endl;
        std::cerr << e.what() << std::endl;
        return 1;
    } catch (ecs::error::ECSError const& e) {
        std::cerr << "An error occured in the game engine:" << std::endl;
        std::cerr << e.what() << std::endl;
        return 1;
    } catch (audio::error::AudioError const& e) {
        std::cerr << "An error occured when using audio:" << std::endl;
        std::cerr << e.what() << std::endl;
        return 2;
    } catch (graphic::error::TextureError const& e) {
        std::cerr << "An error occured when using graphic ressources:" << std::endl;
        std::cerr << e.what() << std::endl;
        return 2;
    }

    return 0;
}
