#include "Graphic/Renderer.hpp"

#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/RenderableImage.hpp"

graphic::Renderer::Renderer(ecs::core::Coordinator& coordinator) :
    imageRenderSystem(coordinator.registerSystem<ecs::system::ImageRenderSystem, ecs::component::Attributes, ecs::component::RenderableImage>())
{
}

void graphic::Renderer::display(sf::RenderWindow& window) const noexcept
{
    this->imageRenderSystem.update(window);
}
