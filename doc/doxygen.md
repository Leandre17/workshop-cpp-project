# Doxygen

[Doxygen](https://doxygen.nl/) is a tool that can generate documentation on your code.  
**You must use this tool when you are writing your code.**

You need to implement the doxygen syntax for all your `.hpp` files.

This document is here to show you how to write this inside code documentation.

## What you should document

After thinking of your code architecture, when you're writing the headers and definitions, that's when you should write this documentation.

Writing a documentation with doxygen is just done by commenting your templates, types, classes and functions definitions.  
Doxygen do all the work for you when this comments are written. The tool generate a documentation that regroup all headers file in a web page.

You can use [markdown syntax](https://www.markdownguide.org/basic-syntax/) to write your comments.

You can generate todos inside doxygen.  
If you want to add an item to the todo, simply add a line like this
```cpp
/**
 * @todo Details of your todo
 */
```

Here is a list of what you should write documentation for:
- [Files](#files)
- [Namespaces](#namespaces)
- [Structures](#structures)
- [Enums / Enums classes](#enums)
- [Classes](#classes)
- [Functions / Methods](#functions)
- [Templates](#templates)
- [Classes attributes](#attributes)

So yeah, you need to write documentation for pretty everything.

## How to write documentation according to the type

Doxygen comments always have the same syntax, it's juste multiline comments like the following:
```
/**
 * @Write your
 * documentation here
 */
```

Even if the documentation syntax is the same for everything, specific keywords should be used for each type of documentation.

Doxygen keywords start with an `@` before a word, i.e `@brief`

---
### Files

When creating a file, you should write a litle header to describe why your file exist.

#### Keywords

| Keyword |                           Description                           |Required|
|---------|-----------------------------------------------------------------|--------|
|`file`   |Name of the file                                                 |`yes`   |
|`brief`  |A litle description of the file and what it contains             |`yes`   |
|`details`|Add additional informations to the brief                         |`no`    |
|`pre`    |Add a precondition information                                   |`no`    |
|`warning`|Add a warning message                                            |`no`    |
|`author` |The author of the file (the one that write doxygen documentation)|`no`    |
|`version`|The version of the file                                          |`no`    |
|`see`    |Additional links                                                 |`no`    |

#### Example

```cpp
/**
 * @file ASystem.hpp
 * @brief System abstraction for ECS systems
 * @details This abstraction follow the 3/5/0 rule
 * @author curs3w4ll
 * @version 1
 */

// File content
```

---
### Namespaces

When creating a namespace, you should write some comments on it.

Their is no need to write the same namespaces documentation each time you use the namespace.

#### Keywords

|  Keyword  |  Description                                     |Required|
|-----------|--------------------------------------------------|--------|
|`namespace`|The name of the namespace                         |`yes`   |
|`brief`    |A litle description of what the namespace contains|`yes`   |
|`details`  |Add additional informations to the brief          |`no`    |
|`pre`      |Add a precondition information                    |`no`    |
|`warning`  |Add a warning message                             |`no`    |
|`see`    |Additional links                                                 |`no`    |

#### Example

```cpp
/**
 * @namespace mynsp
 * @brief This namespace is used for my personal functions
 * @details This functions have been coded by me
 */
namespace mynsp {
    // code...
}
```

---
### Structures

When designing structures, you should document the main container and everything it contains.

#### Keywords

| Keyword |  Description                                     |Required|
|---------|--------------------------------------------------|--------|
|`struct` |The name of the structure                         |`yes`   |
|`brief`  |A litle description of what the structure is for  |`yes`   |
|`details`|Add additional informations to the brief          |`no`    |
|`pre`    |Add a precondition information                    |`no`    |
|`warning`|Add a warning message                             |`no`    |
|`see`    |Additional links                                  |`no`    |

You should also document the variables inside the struct as [attributes](#attributes).

#### Example

```cpp
/**
 * @struct position
 * @brief A structure to store position on two axis
 */
struct position {
    /**
     * @var x
     * @brief The position on the x axis
     */
    int x;
    /**
     * @var y
     * @brief The position on the y axis
     */
    int y;
}
```

---
### Enums

When defining enums, you should just talk about what the enum could be used for.

#### Keywords

| Keyword |  Description                                        |Required|
|---------|-----------------------------------------------------|--------|
|`enum`   |The name of the enum                                 |`yes`   |
|`brief`  |A text that describe what this enum could be used for|`yes`   |
|`details`|Add additional informations to the brief             |`no`    |
|`pre`    |Add a precondition information                       |`no`    |
|`warning`|Add a warning message                                |`no`    |
|`see`    |Additional links                                                 |`no`    |

#### Example

```cpp
/**
 * @enum COLORS
 * @brief An enum that can be used to define colors, and use it as argument
 * @pre The scope where this enum is used should be able to have 
 */
enum class COLORS {
    WHITE,
    RED,
    BLUE,
    GREEN,
    BLACK,
}
```

---
### Classes

Classes are the most important to document with doxygen.  
They are complex objects with sometimes a lot of methods and attributes that you should document.

You should also document every methods as [functions](#functions) and every [attributes](#attributes).

#### Keywords

| Keyword |  Description                           |Required|
|---------|----------------------------------------|--------|
|`class`  |The name of the class                   |`yes`   |
|`brief`  |A text that describe what the class does|`yes`   |
|`details`|Add additional informations to the brief|`no`    |
|`pre`    |Add a precondition information          |`no`    |
|`warning`|Add a warning message                   |`no`    |
|`see`    |Additional links                                                 |`no`    |

#### Example

```cpp
/**
 * @class Bitset
 * @brief Storage to create binary level combinaison
 */
class Bitset {
    // code...
};
```

---
### Functions

Every functions you write should be documented on top of their prototypes.

**:warning: Warning: When overriding a function that have pattern matching, you should NOT use the `fn` keyword.**

#### Keywords

| Keyword |  Description                                          |Required|
|---------|-------------------------------------------------------|--------|
|`fn`     |The name of the function                               |`no`    |
|`brief`  |A text that describe what the function does            |`yes`   |
|`details`|Add additional informations to the brief               |`no`    |
|`param`  |Explain what a parameter is                            |`no`    |
|`return` |The return value of the function if any                |`no`    |
|`throw`  |Exceptions explanation of what the function could throw|`no`    |
|`pre`    |Add a precondition information                         |`no`    |
|`warning`|Add a warning message                                  |`no`    |
|`see`    |Additional links                                                 |`no`    |

#### Example

```cpp
/**
 * @fn sum
 * @brief Do the sum of two numbers
 * @param x The first number to sum
 * @param y The second number to sum
 * @return The sum of the two numbers
 * @throw std::out_of_range thrown if one of the numbers is too big
 */
int sum(int x, int y) {
    // code...
}
```

---
### Templates

You should document templates as well, explaining what the template parameters are and why do you use a template.

#### Keywords

| Keyword |  Description                              |Required|
|---------|-------------------------------------------|--------|
|`brief`  |A text that describe why the template exist|`yes`   |
|`details`|Add additional informations to the brief   |`no`    |
|`tparam` |Explain what a template parameter is       |`no`    |
|`pre`    |Add a precondition information             |`no`    |
|`warning`|Add a warning message                      |`no`    |
|`see`    |Additional links                                                 |`no`    |

#### Example

```cpp
/**
 * @brief This template return the type `T` name for the compiler
 * @tparam T The type to get the name of
 */
template <typename T>
T getTypeName();
```

---
### Attributes

Document what the attributes is and what he will be used for.

Potentially describe which values this variable can take.

#### Keywords

| Keyword |  Description                                |Required|
|---------|---------------------------------------------|--------|
|`var`    |Name of the variable                         |`yes`   |
|`brief`  |A text that describe what the var is used for|`no`    |
|`details`|Add additional informations to the brief     |`no`    |
|`pre`    |Add a precondition information               |`no`    |
|`warning`|Add a warning message                        |`no`    |
|`see`    |Additional links                                                 |`no`    |

#### Example

```cpp
/**
 * @var name The name of the component
 */
std::string name;
```
