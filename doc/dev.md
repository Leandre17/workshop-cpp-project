# Dev section

- [Contribution guidelines](#contribution-guidelines)
- [Conventions](#conventions)
    - [Branch convention](#branch-naming-convention)
    - [Commit convention](#commit-convention)
    - [Coding convention](#coding-convention)
- [ECS Documentation](#ecs-documentation)
    - [ECS Core](#how-does-the-ecs-work-%3F)
    - [Creating new components](#how-to-do-new-components)
    - [Creating new systems](#how-to-do-new-systems)
    - [Main modules](#modules)
- [Engine features](#engine-features)
- [Code Documentation (Doxygen)](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/index.html?job=ExportDoxygen)
    - [How to write Doxygen documentation](doxygen.md)
- [Network Documentation](#network-documentation)

## Contribution guidelines

If you want to contribute to this project, go check the [contribution guidelines](../CONTRIBUTING.md).

### Install for developpers purposes

Download the project from the github and launch the `./devToolsSetup.sh` command to install the tools that are used in this project.  
This tools are here to help you and ease your work.

You also have two scripts to help you use [clang-format](https://clang.llvm.org/docs/ClangFormat.html) and [clang-tidy](https://clang.llvm.org/extra/clang-tidy/).

Use `./clang-format-checker.sh` to format your code following the convention writted for the project.

Use `./clang-tidy-checker.sh` to show you warning about your code.  
Please fix every warnings before pushing your code.

## Conventions

When contributing to this project, you need to follow a set of rules.

This rules are simple and here to help you keep being organized and facilitate the communication with the team.

First of all, when you want to do something on the project, you should watch the [project issues](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/boards).  
You can take any issue that is not already assigned to someone.  
When you want to start working on an issue, you should assign it to you.

### Branch naming convention

When you want to start working on an issue and start coding, you must create a new branch.

Most of the newly created branches are from the [dev branch](/dev).

When you create a new branch, you can't just named it `mybranch` or `branchToDoThat`.  
You **need** to follow the following convention.

Branches names should look like this
`<issue-number>_<parent-branch>_<issue-summary>`

#### `issue-number`

This is the id of the issue this branch is created to work on.

#### `parent-branch`

The name of the branch this branch is created from (and often will be merged on).

#### `issue-summary`

Most of the time is the title of the issue `-` separated.  
Can also being completed with a short issue description.

#### Example

Branch to do issue #13 and that is from `dev` branch to create a clock.

`13_dev_Create-clock`

### Commit convention

To help the team easily keep an eye on everyone advancement, commits must be clear and detailed.

Commits should look like this

`<type>[(scope)]: <description> [#<issue-number>]`

`[body]`

Elements inside `[]` are optional

#### `type`

The type of the modification you are commiting.

For example, if you are adding a new functionality it will be `feat`.

If you are solving a bug it will be `fix`.

Here is a list of `type` keyword:

- `feat`: adding of a new functionality / new library
- `fix`: Resolving a problem (bug, wrong display)
- `doc`: Adding new documentation
- `refact`: Refacting a part of code / all the code
- `test`: Adding tests
- `improvment`: Upgrading an existing functionality

#### `scope`

The scope is optionnal, it's an indication of the part of code / functionality your commiting into.

For exemple, if you're adding a ci you can use: `feat(ci)`.

#### `description`

The description is a brief text that describe what you are committing.

#### `issue-number`

The issue number allows to link your commit to an issue.

It's also on what part of the code your working.

#### `body`

The body is an optionnal additionnal information to add supports (links, images, ...).

Or just describe in details the problem you are fixing.

#### Examples

##### Commiting a new language on an application

`feat(language): Adding of the russian language #14`.

`Adding the russian option for the translation of the home page`.

##### Commiting a bugfix

`fix(keyboard): Removing the blinking effect when pressing keys #54`.

##### Commiting new tests

`test(authentication): Adding full test on the authentication`.

To help you on commit convention, you can execute the `./devToolsSetup.sh` script.  
This will install a script that automatically check your commits.

### Merge requests convention

When creating a merge request, it creates a commit.  
This commit must look like the following:  
`merge(<source_branch>): into <target_branch> #<issue_number>`  

Where `source_branch` is the branch we merge from and `target_branch` is the branch we merge on.  
`issue_number` is the issue id that the merge request should close.  

#### Example

Merging the branch `13_dev_Create-clock` into `dev` and close the #13 issue.

```
merge(13_dev_Create-clock): into dev #13
```

To help you on merge requests, you can execute the `./devToolsSetup.sh` script.  
This will install a script that automatically give you a link to automatically create a merge request into `dev` branch.

---

### Coding convention

The project follows several code conventions so that you can follow them too.

- Use include in `.cpp` files when you can, add the includes in the `.hpp` files as less as possible
- Use doxygen when writing your code, [here](doxygen.md) is how to write doxygen documentation

#### File naming

Files and folders names need to follow the [PascaleCase](https://techterms.com/definition/pascalcase).

#### Type declarations

Type declarations are in [PascaleCase](https://techterms.com/definition/pascalcase) too, as for example Classes, Structures or Typenames.  
An exception of that is the *Namespaces*, Namespaces should be named following the [LowerCamelCase](https://techterms.com/definition/camelcase).

##### Exemples

```cpp
class ExampleCode {
    ExampleCode()
    ~ExampleCode()
};
```

```cpp
struct ExampleStructure {
    int x;
    int y;
};
```

#### Statement declarations

Variables and functions names are in [camelCase](https://techterms.com/definition/camelcase).

```cpp
void exampleFonction() {
    int exampleNumber = 42;
    std::string exampleText = "Hello World"
}
```

---

So that's for conventions to follow when writing code, let's move on to the rules we will follow.

<details>
<summary>Always put brackets for instructions even if they only have one line inside</summary>

##### Bad practice

```cpp
while (condition)
    /*one line of code*/

if (condition)
    /*one line of code*/
``` 

###### Good pratice

```cpp
while (condition) {
   /*one line of code*/
}

if (condition) {
   /*one line of code*/
}
```

</details>

<details>
<summary>Use string_view instead of string</summary>

When possible use [std::string_view](https://en.cppreference.com/w/cpp/string/basic_string_view) instead of [std::string](https://en.cppreference.com/w/cpp/string) for performances reasons.

##### Bad pratice

```cpp
int main() {
    std::string s = "Hello World";
    printString(s);
}

void printString(std::string s) {
    // Code
}
```

##### Good pratice

```cpp
int main() {
    std::string s = "Hello World";
    printString(s);
}

void printString(std::string_view s) {
    // Code
}
```

</details>

<details>
<summary>Avoid virtualization as much as possibles to optimize the code</summary>

When possible, do not use virtual classes heritage.

</details>

### Tips and tricks

Here are some tips to make your life easier when using the project conventions.

#### Issue auto-closing

Dev scripts are here to help you, install them by executing `./devToolsSetup.sh` !

You can close automatically an issue using the `Close` keyword and [all its declinations](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#default-closing-pattern).  
Simple write in the body of your commit `Close #12, #6` will close issue 12 and 6 when merged on main.

## ECS documentation

## How does the ECS work ?

First, you could read [this article](https://austinmorlan.com/posts/entity_component_system) we based our ECS on for more informations.

The ECS is constitued of 3 main parts:
- The entities
- The components
- The systems

An ECS is a game engine concept that does not use virtualization.

Most of c++ game engines use virtualization and heritage to allow multiple objects to have the same functionalities.  
Another type of game engine is ECS (Entity Component System), this game engines do not use heritage but combinaison.  
The goal is to easily create complex objects without having an insane heritage tree.

Here is an example

![A diagram that explain how the ECS works](img/ECSDiagram.png)

We can see that every entities (left column) are associated to components, and every systems (top line) act on a specific set of components.  
Then an entity that have an _Attributes Component_  and a _Transform Component_, for example the `Player` entity, will be modified by the _Movement System_. This entity will also be modified by the _Control System_ to let the user move his player.  
We can see that the `Ennemy` entity is affected by the _Movement System_ too, but this entity should not be affected by the user input, so the `Ennemy` entity is not linked to the _Control System_.

We talked about _entities_, _components_ and _systems_, but what are these parts ?

### The entities

An entity is a just an ID that represent something, this could be a player, a wall, a text, anything.

In reality this entity of type [`ecs::core::Entity`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/structecs_1_1core_1_1Entity.html?job=ExportDoxygen) is just a unique number that represent something.  
This [`ecs::core::Entity`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/structecs_1_1core_1_1Entity.html?job=ExportDoxygen) is used to identify each entity in every part of the ECS.

### The components

A component is a mass of data that caracterise an entity.  
This data could be of any type, a structure, a class...

Each entity has its own components, with datas associated to.  
Components should just be caracteristics of the entities.  
For example, a position is a component, a full example is given at the end of the explanation. 

### The systems

A system is an object that make modification on the entities components.  
Each system modify each entity that match the a specific condition to be associated with this system.

Each entity has it own _signature_.  
This signature is a bitset (`0010101101`) with each bit representing a component and each bit set to `1` mean that the entity has and use the component registered to this bit.  
You don't have to manipulate this bitset, the ECS does it for you.

All you have to remember is that you need to indicate to the ECS that you want your entities to be able to have a certain component.  
For that you need to use the function [`registerComponent`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classecs_1_1core_1_1Coordinator.html?job=ExportDoxygen#a5c706383875e09f53ced998f15861c40) from the ECS.  
This function will bind a slot of the bitset to your component.

Now that you know what a signature is (do not hesitate to read what is a signature again), I can easily describe you how the ECS work.  
In this example we will assume that the component `Position` is just an `int x` and an `int y` and the component `Movement` is also an `int x` and an `int y`.  
First, you have to register the component `Position` in the ECS, now the bit at position 0 (_furthest to the right_ position) is bind to the `Position` component.  
Same for the component `Movement`, now the bit at position 1 (_furthest to the right - 1_ position) is bind to the `Movement` component.  
Then you can create three entities, `A`, `B` and `C`.  
```cpp
Entity A = coordinator.createEntity();
Entity B = coordinator.createEntity();
Entity C = coordinator.createEntity();
```
You can assign the component `Position` to entity `A`, its signature will be `10`.  
```cpp
coordinator.setComponent<Position>(A, Position{0, 0});
```
You can assign the component `Position` and `Movement` to entity `B`, its signature will be `11`.  
```cpp
coordinator.setComponent<Position>(B, Position{10, 10});
coordinator.setComponent<Position>(B, Position{2, 2});
```
We will not assign any component to entity `C`.  

**So now, the systems.**  
A system will act on every entity that contain the system signature.  
For example a system with the signature `00110`.  
- will apply on an entity with the signature `00110`.
- will also apply on an entity with the signature `10110`.
- but will not apply on an entity with the signature `10100`.

Because the bits at position 2 and 3 are set to `1` in the two first entities.

Taking the previous example, we will create a system `MovementSystem` with the mission to add the `x` and the `y` contained in the `Movement` component to the `x` and the `y` contained in the `Position` component.

For that we first have to, like the components, register the system in the ECS.  
A system is a class inheriting from the [`ASystem`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classecs_1_1core_1_1ASystem.html?job=ExportDoxygen) class.  
```cpp
class MovementSystem final : public ASystem {
    void update() {
        // code that add movement to position
    }
}
```

Then we have to set the signature of the system, this will define which component the system will operate on.
```cpp
auto& movSys = coordinator.registerSystem<MovementSystem, Position, Movement>();
```
`MovementSystem` is the type of the system defined just before.  
Every other type arguments are the components defining the system signature.  
That mean this `MovementSystem` will act on every entities that have a `Position` component and `Movement` component associated with.
```cpp
Entity A = coordinator.
```

Assume that entity `A` has the following components:
```cpp
Position {
    int x = 10; 
    int y = 30; 
};

Movement {
    int x = 2;
    int y = 5;
};
```
```cpp
Entity A = coordinator.createEntity();
coordinator.setComponent<Position>(Position{10, 30});
coordinator.setComponent<Movement>(Movement{2, 5});
```
When we apply (or update) the `MovementSystem` on the entity `A`.
```cpp
movSys.update();
```

We will then have.
```cpp
Position {
    int x = 12; 
    int y = 35; 
};

Movement {
    int x = 2;
    int y = 5;
};
```

And that's it !  
This is how to ECS works.  
This example was a simple system and simple components, but you can make much more complex ones.

Here is a litle view of how the ECS is structured.

![ECS Module diagram](img/ECSStructure.png)

### How to do new components

First of all, I hope you've read [ECS Documentation](#ecs-documentation) before reading this :grimacing:.

If you want to create a new component, just write the structure or the class that is representing your component.  
This object should be inside the [`ecs::component`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/namespaceecs_1_1component.html?job=ExportDoxygen) namespace.

With that done, just register your component into the ECS coordinator like the following.

```cpp
coordinator.registerComponent<ecs::component::<YourComponent>();
```

Then you can add your component to entities.

```cpp
coordinator.registerComponent<ecs::component::<YourComponent>();

ecs::core::Entity e = coordinator.createEntity();
coordinator.setComponent<ecs::component::<YourComponent>>(e, {1, 5});
auto& c = coordinator.getComponent<ecs::component::<YourComponent>>(e);
```

### How to do new systems

Same as components, please read the [ECS Documentation](#ecs-documentation) before reading this.

When creating a system, you should create a class inheriting from [`ecs::core::ASystem`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classecs_1_1core_1_1ASystem.html?job=ExportDoxygen).  
This allow the ECS to use your system without knowing its specific type.

You do not have to register and unregister entities from your system, the ECS does it for you.

You can access every entities linked to your system with the `entities` attribute.  
This attribute is a [`std::set`](https://en.cppreference.com/w/cpp/container/set) inherited from [`ASystem`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classecs_1_1core_1_1ASystem.html?job=ExportDoxygen) class.

You can access to the components of every entities on this set. Each entity contained in this set is guaranteed to have the component that constitute the system signature.

For example, here is a basic system.

```cpp
namespace ecs::system {
    class MovementSystem final : public ASystem {
        ASystem() noexcept = default;
        void update(ecs::core::Coordinator& c) {
            for (const auto& e : this->entities) {
                auto& pos = c.getComponent<ecs::component::Position>(entity);
                auto& transform = c.getComponent<ecs::component::Transform>(entity);

                pos.x += transform.x;
                pos.y += transform.y;
            }
        }
    }
}
```

Registering the system

```cpp
auto& movSys = coordinator.registerSystem<ecs::system::MovementSystem>();

ecs::core::Entity e = coordinator.createEntity();
coordinator.setComponent<ecs::component::Position>(e, {10, 15});
coordinator.setComponent<ecs::component::Transform>(e, {1, 2});

// entity position : { x: 10, y: 15 }, transfrom: { x: 1, y:2 }

movSys.update(coordinator);

// entity position : { x: 11, y: 17 }, transfrom: { x: 1, y:2 }

movSys.update(coordinator);
movSys.update(coordinator);

// entity position : { x: 13, y: 21 }, transfrom: { x: 1, y:2 }
```

## Modules

A module is the set of a system and a component working together.

For example the `Bounds` component and the `Collision` system are part of the same module that manages the boxes and the collisions of the different entities in the game.

<details>
<summary>Image Renderer Module</summary>

### Image Renderer

##### `Renderable Image` Component

```cpp
class RenderableImage {
   public:
    RenderableImage() noexcept = default;
    explicit RenderableImage(std::string_view imagePath, Vector position) noexcept;
    RenderableImage(RenderableImage const& other) noexcept = default;
    RenderableImage(RenderableImage&& other) noexcept;
    ~RenderableImage() noexcept = default;

    RenderableImage& operator=(RenderableImage const& other) noexcept = default;
    RenderableImage& operator=(RenderableImage&& other) noexcept;

    /*
     * Get the sprite     
     *
     * Return a reference to the sprite 
     */
    const sf::Sprite& getSprite() const;
    /*
     * Set a new texture for the image
     *
     * No return value
     *
     * imagePath -> The path to the image file
     */
    void setTexture(std::string_view imagePath) noexcept;
    /*
     * Get a texture of the image
     * 
     * Return the texture of the image 
     */
    sf::Texture getTexture() const noexcept;
    /*
     * Get the size of the image
     *
     * Return the size as a vector (only x and y count)
     */
    Vector getSize() const noexcept;

   private:
    sf::Texture texture{};
    sf::Sprite sprite{};
    bool valid = false;
};
```

##### `Renderer Image` System

```cpp
class ImageRenderSystem final : public System {
   public
    ImageRenderSystem() noexcept = default;
    ImageRenderSystem(ImageRenderSystem const& other) noexcept = default;
    ImageRenderSystem(ImageRenderSystem&& other) noexcept = default;
    ~ImageRenderSystem() noexcept final = default;

    ImageRenderSystem& operator=(ImageRenderSystem const& other) noexcept = default;
    ImageRenderSystem& operator=(ImageRenderSystem&& other) noexcept = default;

    /*
     * Update the entities linked to the system
     *
     * No return value
     *
     * coordinator -> The coordinator to use to update the system
     *
     * THIS FUNCTION SHOULD BE CALLED INSIDE A 'BeginDrawing' FUNCTION CALL
     */
    void update(Coordinator& coordinator, sf::Window &window) const;
};
```
</details>

<details>
<summary>Text Renderer Module</summary>

### Text Renderer

##### `Text Renderer` Component

```cpp
class RenderableText {
   public:
    RenderableText() noexcept = default;
    explicit RenderableText(std::string_view content, std::string_view fontPath, unsigned int characterSize = 30) noexcept;
    RenderableText(std::string_view content, std::string_view fontPath, unsigned int characterSize = 30, float lineSpacing = 1, float letterSpacing = 1, int style = sf::Text::Regular, sf::Color fillColor = sf::Color::White, sf::Color outLine = sf::Color::White) noexcept;
    RenderableText(RenderableText const& other) noexcept = default;
    RenderableText(RenderableText&& other) noexcept;
    ~RenderableText() noexcept = default;

    RenderableText& operator=(RenderableText const& other) noexcept = default;
    RenderableText& operator=(RenderableText&& other) noexcept;

    /*
     * Draw the text
     *
     * No return value
     *
     * position -> The position where to draw the text
     */
    void draw() const noexcept;

   private:
    bool needDestroy = false;
    sf::Text text;
    sf::Font font{};
    std::string content{};
    unsigned int characterSize = 30;
    float letterSpacing = 1;
    float lineSpacing = 1;
    int style = sf::Text::Regular;
    sf::Color fillColor = sf::Color::White;
    sf::Color outColor = sf::Color::White;
};
```

##### `Renderer Text` System

```cpp
class TextRenderSystem final : public System {
   public
    TextRenderSystem() noexcept = default;
    TextRenderSystem(TextRenderSystem const& other) noexcept = default;
    TextRenderSystem(TextRenderSystem&& other) noexcept = default;
    ~TextRenderSystem() noexcept final = default;

    TextRenderSystem& operator=(TextRenderSystem const& other) noexcept = default;
    TextRenderSystem& operator=(TextRenderSystem&& other) noexcept = default;

    /*
     * Update the entities linked to the system
     *
     * No return value
     *
     * coordinator -> The coordinator to use to update the system
     *
     * THIS FUNCTION SHOULD BE CALLED INSIDE A 'BeginDrawing' FUNCTION CALL
     */
    void update(Coordinator& coordinator, sf::Window &window) const;
};
```
</details>

<details>
<summary>Animation Module</summary>

### Animation

##### `Animation` Component

```cpp
class Animation {
   public:
    Animation() noexcept = default;
    explicit Animation(const sf::Sprite &sprite, const Vector &frameSize, unsigned int fps = 60);
    Animation(Animation const& other) noexcept = default;
    Animation(Animation&& other) noexcept;
    ~Animation() noexcept;

    Animation& operator=(Animation const& other) noexcept = default;
    Animation& operator=(Animation&& other) noexcept;

    void doAnimation();

   private:
    bool valid = false;
    unsigned int fps;
    unsigned int frame;
    Vector frameSize;
    sf::Sprite sprite;
    sf::Clock clock;
};
```

##### `Animation` System

```cpp
class AnimationSystem final : public System {
   public
    AnimationSystem() noexcept = default;
    AnimationSystem(AnimationSystem const& other) noexcept = default;
    AnimationSystem(AnimationSystem&& other) noexcept = default;
    ~AnimationSystem() noexcept final = default;

    AnimationSystem& operator=(AnimationSystem const& other) noexcept = default;
    AnimationSystem& operator=(AnimationSystem&& other) noexcept = default;

    /*
     * Update the entities linked to the system
     *
     * No return value
     *
     * coordinator -> The coordinator to use to update the system
     *
     * elaspedTime -> The time elapsed since we want to update (in seconds)
     */
    void update(Coordinator& coordinator, float elaspedTime) const;
};
```
</details>

<details>
<summary>Attributes Module</summary>

### Attributes

##### `Attributes` Component

```cpp
struct Attributes {
    Vector position{0, 0};
    Vector scale{0, 0};
    Vector origin{0, 0};
    float angle = 0;
};
```

##### `Transform` System

```cpp
class TransformSystem final : public System {
   public:
    TransformSystem() noexcept = default;
    TransformSystem(TransformSystem const& other) noexcept = default;
    TransformSystem(TransformSystem&& other) noexcept = default;
    ~TransformSystem() noexcept final = default;

    TransformSystem& operator=(TransformSystem const& other) noexcept = default;
    TransformSystem& operator=(TransformSystem&& other) noexcept = default;

    /*
     * Update the entities linked to the system
     *
     * No return value
     *
     * coordinator -> The coordinator to use to update the system
     *
     * elapsedTime -> The time elapsed since we want to update (in seconds)
     */
    void update(Coordinator& coordinator, float elapsedTime) const noexcept;
};
``` 
</details>

<details>
<summary>Collision Module</summary>

### Collision

##### `Bounds` Component

```cpp
class Bounds
{
    public:
        Bounds() noexcept = default;
        explicit Bounds(const sf::Sprite &sprite);
        Bounds(Bounds const& other) noexcept = default;
        Bounds(Bounds&& other) noexcept;
        ~Bounds() noexcept;

        sf::FloatRect getBounds() const;
        
        bool intersects(cosnt sf::FloatRect &rect) const;
        bool contains(const Vector &point) const;

    private:
        sf::FloatRect bounds;
};
```

##### `Collision` System

```cpp
class CollisionSystem final : public System {
   public:
    CollisionSystem() noexcept = default;
    CollisionSystem(CollisionSystem const& other) noexcept = default;
    CollisionSystem(CollisionSystem&& other) noexcept = default;
    ~CollisionSystem() noexcept final = default;

    CollisionSystem& operator=(CollisionSystem const& other) noexcept = default;
    CollisionSystem& operator=(CollisionSystem&& other) noexcept = default;

    /*
     * Remove an entity from to the system
     *
     * No return value
     *
     * entity -> The entity to remove from the system
     */
    void removeEntity(Entity entity) noexcept final;

    /*
     * Set the entity as a non-moving object
     *
     * No return value
     *
     * entity -> The entity to set non-moving
     */
    void setEntityNotMoving(ecs::core::Entity entity) noexcept;
    /*
     * Update the entities linked to the system
     *
     * No return value
     *
     * coordinator -> The coordinator to use to update the system
     */
    void update(Coordinator& coordinator) const noexcept;

   private:
    std::set<Entity> notMovingEntities;
};
``` 
</details>

Here is a list of the main components and systems our ECS has.

![ECS architecture](img/ECSArchitecture.png)

Here is a list of the main modules and what they are constitued of

![ECS Module diagram](img/ECSModules.png)

## Engine features

The game engine we use allow you more than just providing an ECS.

### Callable

[`Callback`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classutils_1_1Callback.html?job=ExportDoxygen) is an object that allow you to create function pointer that you can call at any time.

This object is used in many features, like collisions, animations callbacks, key pressed/released events and others.

To use [`Callback`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classutils_1_1Callback.html?job=ExportDoxygen), you should construct the Callback object by passing a function pointer and arguments.

Here is some use example

```cpp
void display() {
    // code
}

utils::Callback c(&display);
c(); -> call display function
```

```cpp
void display(int i) {
    // code
}

utils::Callback c(&display, 4);
c(); -> call display function with 4 as argument
```

```cpp
void display(int i) {
    // code
}

int i = 2;
utils::Callback c(&display, i);
i = 5;
c(); -> call display function with 5 as argument
```

```cpp
class A {
    display() {
        // code
    }
};

A a;
utils::Callback c(&A::display, &a);
c(); -> call display method of `a` object
```

```cpp
class A {
    display(int i) {
        // code
    }
};

A a;
utils::Callback c(&A::display, &a, 3);
c(); -> call display method of `a` object with 3 as argument
```

### Collisions

The [`CollisionSystem`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classecs_1_1system_1_1CollisionSystem.html?job=ExportDoxygen) allow you to detect when 2 entities touch each other.

Each entity that have a [`Collider`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classecs_1_1component_1_1Collider.html?job=ExportDoxygen) component, that can trigger a callback function when colliding with another entity.

A collider is caracterised by its types.  
This types can be whatever you want, as long as it's castable into an `int`.  
Most of the type, the different types of colliders will be listed in an enum like the following
```cpp
enum class CollidersTypes {
    DAMAGEABLE = 1,
    DAMAGER = 2,
    OBSTACLE = 4,
};
```
This enum is only an example, it is not defined in the game engine.  
For a real use, a collider type is defined at its creation, inside the constructor.  
Collider constructor arguments are the different types that caracterise the collider.  
As said before, this arguments you use as types can be whatever you want as long as they are castable into an `int`.  
So most of the type you define an enum like previously and use it.  
The reason why the previous enum use multiples of 2 as values is to do [composition](https://www.codeproject.com/Articles/13740/The-Beginner-s-Guide-to-Using-Enum-Flags). So with the type of a collider (which is an `int`, the sum of all arguments you passed in its constructor), you can extract each type (members of the enum) that compose the global collider type.  
Example:
```cpp
enum class CollidersTypes {
    DAMAGEABLE = 1,
    DAMAGER = 2,
    OBSTACLE = 4,
};

Collider c(CollidersTypes::DAMAGEABLE, CollidersTypes::OBSTACLE);

int type = c.getType();

if ((type & static_cast<int>(CollidersTypes::DAMAGEABLE)) > 0) {
    // The type is constitued of DAMAGEABLE
    // In this case: true
}
if ((type & static_cast<int>(CollidersTypes::DAMAGER)) > 0) {
    // The type is constitued of DAMAGER
    // In this case: false
}
if ((type & static_cast<int>(CollidersTypes::OBSTACLE)) > 0) {
    // The type is constitued of OBSTACLE
    // In this case: true
}
```

You can compose each entity collider with this types, a single entity can combine types, it can be DAMAGER and OBSTACLE as previously.  

To use the collider, you will need to trigger a function when the collider collide with another.

To do that, use the `setCallback` method to add a function to call when colliding with another collider, example as following
```cpp
void collidingFun(ecs::core::Entity const& otherEntity, ecs::component::Collider const& otherCollider, utils::Rect<float> collidingFaces) {
    std::cout << "Im colliding with something (entity " << entity << ")" << std::endl;
    if ((type & static_cast<int>(CollidersTypes::OBSTACLE)) > 0) {
        std::cout << "The thing I'm colliding with is an obstacle, should I stop ?" << std::endl;
        // code to stop for example
    }
}
Collider c(CollidersTypes::DAMAGEABLE, CollidersTypes::OBSTACLE);
c.setCallback(&collidingFun);
```
Here we sayed that when our collider `c` collide with another collider, the collision system will call the function `collidingFun`.  
We can also call a function with predefined arguments.  
For example if the collider is the one of a player:
```cpp
void collidingFun(game::Player& player, ecs::core::Entity const& otherEntity, ecs::component::Collider const& otherCollider, utils::Rect<float> collidingFaces) {
    std::cout << "Im colliding with something (entity " << entity << ")" << std::endl;
    if ((type & static_cast<int>(CollidersTypes::OBSTACLE)) > 0) {
        std::cout << "The thing I'm colliding with is an obstacle, should I stop ?" << std::endl;
        // code to stop for example
    }
}

game::Player player;
Collider c(CollidersTypes::DAMAGEABLE, CollidersTypes::OBSTACLE);
c.setCallback(&collidingFun, std::ref(player));
```
Note that we keep the three last arguments, but added a new argument before them, this is "captured arguments", arguments that are defined and provided at the callback creation.  

Note that the function you define as a collider should ALWAYS take `ecs::core::Entity const&, ecs::component::Collider const&, utils::Rect<float>` as three last arguments, this arguments are given by the collision system to all colliders callbacks.  
This three arguments are:
- `otherEntity`: The entity we collide with
- `otherCollider`: The collider object of the other entity we collided with
- `collidingFaces`: The faces that collide and on which pixels, defined as follow:
    - top: The number of pixels the other collider was from the top of this collider
    - bottom: The number of pixels the other collider was from the bottom of this collider
    - left: The number of pixels the other collider was from the left of this collider
    - right: The number of pixels the other collider was from the right of this collider
    - Examples:

![Collisions Faces definition](img/CollisionsFaces.png)

This is how collidingFaces values are defined.  
Here are some specific cases:
- If the collider 1 is contained entirely inside the collider 2
    - Collider 1 will have all its values > 0
    - Collider 2 will have all its values = 0
    - So that means:
        - If a CollisionsFaces has all its values = 0, the collider is fully containing the other collider
        - If a CollisionsFaces has all its values > 0, the collider is fully contained by the other collider
- If the CollisionsFaces has left|right > 0 and left = right, the collider is contained horizontally by the other collider
- If the CollisionsFaces has top|bottom > 0 and top = bottom, the collider is contained vertically by the other collider
- If the CollisionsFaces has top = 0 and bottom = 0 and left != 0 or right != 0, the collider is containing the other collider vertically
- If the CollisionsFaces has right = 0 and left = 0 and top != 0 or bottom != 0, the collider is containing the other collider horizontally

IMPORTANT: If you want to use a captured argument that is a reference, you MUST use `std::ref` function when passing it to the callback constructor, otherwise you won´t compile.

This collision concept allows you to adapt different behaviour according to the types of entities that are colliding.

To use the collision system in a real case, see following:

Add a [`Collider`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classecs_1_1component_1_1Collider.html?job=ExportDoxygen) component to your entity and set the type of the collider
```cpp
ecs::core::Entity entity = coordinator.createEntity();
auto& collider = coordinator.setComponent<ecs::component::Collider>(entity, ecs::component::CollidersTypes::DAMAGEABLE, ecs::component::CollidersTypes::DAMAGER);

// Set collider callback
collider.setCallback(&SomeRandomFunction);
```

So if the entity collide with another entity, the callback (pointing to function `SomeRandomFunction`) will be called.  

### Window and Renderer

The game engine also provide you a [`Window`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classgraphic_1_1Window.html?job=ExportDoxygen) class that do all rendering etc for you.

You simply have to create a window, with the wanted parameter, and juste some methods.  
Here is a sample main loop
```cpp
ecs::core::Coordinator coordinator;
graphic::Window window(coordinator, "My window");

// Inits

while (window.isOpen()) {
    // code

    window.treatEvents();

    // code

    window.render();
}
```
And you simply have a basic window that can draw images !

This [`Window`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classgraphic_1_1Window.html?job=ExportDoxygen) contain a [`Renderer`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classgraphic_1_1Renderer.html?job=ExportDoxygen) object that make all the graphic render.

### EventHandler

The game engine provide to you a simple class that can treat events that occurs on the window.  
This object is also included in the [previously seen Window](#window-and-renderer) object.

You simply have to set your window up by calling the `setPressedKeyCallback` and `setReleasedKeyCallback` function.  
This function again use [Callbacks](#callable) to trigger function when a specific key is pressed or released.

Here is an example
```cpp
ecs::core::Coordinator coordinator;
graphic::Window window(coordinator, "My window");

// Inits

window.setPressedKeyCallback(sf::KeyBoard::Up, &startMoving);
window.setReleasedKeyCallback(sf::KeyBoard::Up, &stopMoving);

while (window.isOpen()) {
    window.treatEvents();
    // code
}
```

You can add as many callbacks as you want for a specific key.  
This allows you to simply treat the user input on the window.

### Musics and Sounds

If you want to use musics or sounds in your game, the game engine is here for you.

You can create as many [`Music`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classaudio_1_1Music.html?job=ExportDoxygen) objects as you want, they're simply to use and really usefull for playing background music.
```cpp
audio::Music music("myMusicFilePath");
music.play();
music.pause();
music.setVolume(20);
music.play();
music.stop();
```

If you want to use some sounds inside your game that can happen at anytime and that should be spammed, the [`SoundPlayer`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classaudio_1_1SoundPlayer.html?job=ExportDoxygen) is for you.  
This class allow you to use sounds without having to carrying about freeing them or storing them.

Here is how it works
```cpp
audio::SoundPlayer::playSound("mySoundFilePath");
```
As simple as that, this will play your sound.

You also can change the volume of the sounds.  
Either by changing the global sounds volume
```cpp
audio::SoundPlayer::setSoundsVolume(40);
```
Or by changing the volume of a specific sound
```cpp
audio::SoundPlayer::setSoundVolume("mySoundFilePath", 34);
```

### Ressource manager

The engine use a [`ResourceManager`](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/jobs/artifacts/main/file/public/classgraphic_1_1ResourceManager.html?job=ExportDoxygen) that you could modify if needed.

This object is a singleton that manage ressources (like textures, sounds, ...) to avoid duplicated ressources in the memory.

This allow, for example with textures, multiple sprites to use the same texture.

To use the ressource manager, simply get a reference to your ressource like the following (again textures as an example)
```cpp
auto& texture graphic::ResourceManager::getTexture("myTexturePath");
```

## Network documentation

**#TODO**
