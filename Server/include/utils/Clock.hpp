/**
 * @file Clock.hpp
 * @brief The clock class for implement tick system.
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include <chrono>
#include <sys/time.h>

/**
 * @namespace utils
 * @brief all utils function and class for us RType.
 */
namespace utils {

/**
 * @class Clock
 * @brief Clock class for time gestion.
 */
class Clock {
   public:
    Clock() noexcept;
    Clock(Clock const& other) noexcept = default;
    Clock(Clock&& other) noexcept = default;
    ~Clock() noexcept = default;

    Clock& operator=(Clock const& other) noexcept = default;
    Clock& operator=(Clock&& other) noexcept = default;

    /**
     * @brief Save the actual time as a timepoint in the clock
     *
     */
    void saveTimePoint() noexcept;

    /**
     * @brief Return true if the given time (milliseconds) as been elapsed since the last saved timepoint
     *
     * @pre Precondition: The params milliseconds need to be >= 0 and <= 2147483647
     *
     * @param milliseconds The number of milliseconds you want to check
     *
     */
    bool isElapsed(int milliseconds) const noexcept;

    /**
     * @brief Return the time between the last saved timepoint and the actual time.
     *
     */
    float getElapsedTime() const noexcept;

   private:
    /**
     * @brief Get the actual time
     */
    static time_t getActualTime() noexcept;
    time_t timepoint{}; /*!< The timepoint variable*/
};

} // namespace utils

