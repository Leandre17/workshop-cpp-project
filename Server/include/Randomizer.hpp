/**
 * @file Randomizer.hpp
 * @brief Get random numbers
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include <cstddef>
#include <functional>
#include <random>
#include <unordered_map>

namespace utils {

/**
 * @brief Be able to create mix and max numbers for any type
 *
 * @tparam T The type of the numbers
 */
template <typename T>
/**
 * @struct MinMax
 * @brief Contain a minimal number and a maximal number and can be compared
 */
struct MinMax {
    bool operator==(const MinMax& other) const noexcept
    {
        return (this->min == other.min and this->max == other.max);
    }
    /**
     * @var min
     * @brief The minimal value of the pair
     */
    T min = 0;
    /**
     * @var max
     * @brief The maximal value of the pair
     */
    T max = 0;
};

/**
 * @struct MinMaxHash
 * @brief Hasher for MinMax structure
 */
struct MinMaxHash {
    /**
     * @brief Be able to hash on any type
     *
     * @tparam T The type of the pair to hash
     */
    template <class T>
    std::size_t operator()(const MinMax<T>& obj) const noexcept
    {
        std::size_t h1 = std::hash<T>()(obj.min);
        std::size_t h2 = std::hash<T>()(obj.max);

        return h1 ^ h2;
    }
};

/**
 * @class Randomizer
 * @brief Generate random numbers between two numbers
 */
class Randomizer {
   public:
    Randomizer() noexcept = delete;
    Randomizer(Randomizer const& other) noexcept = delete;
    Randomizer(Randomizer&& other) noexcept = delete;
    ~Randomizer() noexcept = delete;

    Randomizer& operator=(Randomizer const& other) noexcept = delete;
    Randomizer& operator=(Randomizer&& other) noexcept = delete;

    /**
     * @brief Reset random seed
     */
    static void resetSeed() noexcept;
    /**
     * @brief Be able to generate random numbers of any type
     *
     * @tparam T The type to generate random number
     */
    template <typename T = int>
    /**
     * @brief Generate a random number between two numbers
     *
     * @param min The minimum value of the generated number (included)
     * @param max The maximum value of the generated number (included)
     *
     * @return The randomly generated number
     */
    static T getRandomNumber(T min, T max) noexcept
    {
        using distribution = std::conditional_t<std::is_integral_v<T>, std::uniform_int_distribution<T>, std::conditional_t<std::is_floating_point_v<T>, std::uniform_real_distribution<T>, void>>;

        static std::random_device rd;
        static std::mt19937 mt(rd());
        static std::unordered_map<MinMax<T>, distribution, MinMaxHash> map;

        if (not map.contains({min, max})) {
            map.insert({{min, max}, distribution(min, max)});
        }
        return map.at({min, max})(mt);
    }
    static bool getRandomNumber() noexcept
    {
        return (getRandomNumber(0, 1) == 0);
    }
};

} // namespace utils
