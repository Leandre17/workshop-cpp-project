/**
 * @file NetworkSerialiser.hpp
 * @brief This file have the serializer to binary
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include "BinaryConverter.hpp"
#include "Bitset.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "SFML/System/Vector2.hpp"

#include <optional>
#include <vector>

/**
 * @brief Byte(8 Bit) on binary level
 */
using Byte = std::uint8_t;
/**
 * @struct playerConnection
 * @brief A structure to store the player connection information
 */
struct playerConnection {
    /**
     * @var clientIp
     * @brief the IPV4 client adress
     */
    std::optional<sf::IpAddress> clientIp;
    /**
     * @var clientPort
     * @brief the user port (1-65535)
     */
    unsigned short clientPort = 0;
    /**
     * @var id
     * @brief The unique id of the player
     */
    unsigned short id = 0;
};
/**
 * @struct playerInfo
 * @brief A structure who store the players data
 */
struct playerInfo {
    /**
     * @var id
     * @brief The unique id for the player
     */
    unsigned short id = 5000;
    /**
     * @var pos
     * @brief the x and y positions of the player
     */
    sf::Vector2f pos{};
    /**
     * @var life
     * @brief the player's life
     */
    unsigned short life = 100;
    /**
     * @var damage
     * @brief the damage of the player
     */
    unsigned short damage = 10;
};

/**
 * @struct enemyInfo
 * @brief the data of one enemy
 */
struct enemyInfo {
    /**
     * @var pos
     * @brief the x and y positions of the enemy
     */
    sf::Vector2f pos{};
    /**
     * @var life
     * @brief the enemy life
     */
    unsigned short life = 100;
    /**
     * @var damage
     * @brief the damage of the enemy
     */
    unsigned short damage = 10;
    /**
     * @var id
     * @brief the unique id of the enemy
     */
    unsigned short id = 0;
    /**
     * @var enemyTypeID
     * @brief the type of the enemy
     * @details The client need to know when he get data from the server witch enemy spawn.
     * @see https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/blob/dev/doc/dev.md
     */
    unsigned short enemyTypeID = 0;
};

/**
 * @struct projectile
 * @brief the data of the projectile
 */
struct projectile {
    /**
     * @var damage
     * @brief the Damage of the projectile
     */
    unsigned short damage = 100;
    /**
     * @var pos
     * @brief the x and y postion of the projectile
     */
    sf::Vector2f pos{};
    /**
     * @var id
     * @brief The unique id of the projectile (0-50000)
     */
    unsigned short id = 0;
    /**
     * @var projectileTypeID
     * @brief The skin id of the projectile
     */
    unsigned short projectileTypeID = 0;
    /**
     * @var ownerId
     * @brief the player id who create the projectile
     */
    unsigned short ownerId = 0;
};

/**
 * @namespace network
 * @brief the main namespace of the network
 */

namespace network {

/**
 * @class NetworkSerialiser
 * @brief the Serializer for the RType who convert player,projectile,enemy => std::vector<Byte>
 */
class NetworkSerialiser {

   public:
    NetworkSerialiser() noexcept = default;
    NetworkSerialiser(NetworkSerialiser const& other) noexcept = default;
    NetworkSerialiser(NetworkSerialiser&& other) noexcept = default;
    NetworkSerialiser& operator=(NetworkSerialiser const& other) noexcept = default;
    NetworkSerialiser& operator=(NetworkSerialiser&& other) noexcept = default;
    ~NetworkSerialiser() noexcept = default;

    /**
     * @fn getOffset
     * @brief This function return the first offset of rowData
     * @param rowData it's the vector who contain binary data
     * @warning This function erase 2 Byte of rowData when call
     * @return return the opcode
     * @see https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/blob/dev/doc/dev.md
     */
    unsigned short getOffset(std::vector<Byte>& rowData) noexcept;

    /**
     * @fn convertPlayerToBinary
     * @brief this function will allow you to convert a struct playerInfo to binary
     * @param player the playerInfo data we want to convert to binary
     * @warning this function add the opcode of the player
     * @return the binary vector of player
     */
    std::vector<Byte> convertPlayerToBinary(struct playerInfo& player) noexcept;
    /**
     * @fn convertBinaryToPlayer
     * @brief This function convert a Byte data to playerInfo
     * @param rowData the vector of Byte
     * @warning rowData OPCODE need to be remove before sending to this fuction
     * @return the playerInfo structutre with the converted data from rowData
     */
    struct playerInfo convertBinaryToPlayer(std::vector<Byte>& rowData) noexcept;

    /**
     * @fn convertEnemyToBinary
     * @brief this function will allow you to convert a struct enemyInfo to binary
     * @param enemy the enemyInfo data we want to convert to binary
     * @warning this function add the opcode of the enemy
     * @return the binary vector of enemy
     */
    std::vector<Byte> convertEnemyToBinary(struct enemyInfo& enemy) noexcept;
    /**
     * @fn convertBinaryToEnemy
     * @brief This function convert a Byte data to enemyInfo
     * @param rowData the vector of Byte
     * @warning rowData OPCODE need to be remove before sending to this fuction
     * @return the enemyInfo structutre with the converted data from rowData
     */
    struct enemyInfo convertBinaryToEnemy(std::vector<Byte>& rowData) noexcept;

    /**
     * @fn convertProjectileToBinary
     * @brief this function will allow you to convert a struct projectile to binary
     * @param projectile the projectile data we want to convert to binary
     * @warning this function add the opcode of the projectile
     * @return the binary vector of projectile
     */
    std::vector<Byte> convertProjectileToBinary(struct projectile& projectile) noexcept;
    /**
     * @fn convertBinaryToProjectile
     * @brief This function convert a Byte data to projectile
     * @param rowData the vector of Byte
     * @warning rowData OPCODE need to be remove before sending to this fuction
     * @return the projectile structutre with the converted data from rowData
     */
    struct projectile convertBinaryToProjectile(std::vector<Byte>& rowData) noexcept;
    /**
     * @fn addSignature
     * @brief Create the OPCODE Binary data
     * @param OFFSET the opcode value
     * @return the vector of opcode Byte
     */
    std::vector<Byte> addSignature(unsigned short OFFSET) noexcept;

   private:
    /**
     * @var dataSizeToRead
     * @brief the number of Byte we have to read
     */
    unsigned short dataSizeToRead = 0;
    /**
     * @var bitset
     * @brief the Bitset class for binary operaitons
     */
    ecs::core::Bitset bitset;
    /**
     * @var bconverter
     * @brief binary converter with basic base (float, int ...)
     */
    BinaryConverter bconverter;
};
} // namespace network
