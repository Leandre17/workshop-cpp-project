/**
 * @file BinaryConverter.hpp
 * @brief Serializer for converting some data to binary
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include "Bitset.hpp"
#include "SFML/System/Vector2.hpp"

#include <algorithm>
#include <string>
#include <vector>

/**
 * @brief Byte(8 Bit) on binary level
 */

using Byte = std::uint8_t;

/**
 * @class BinaryConverter
 * @brief This class can swap many variable type in binary and change the binary format in variable.
 */

class BinaryConverter {
   public:
    BinaryConverter() noexcept = default;
    BinaryConverter(BinaryConverter const& other) noexcept = default;
    BinaryConverter(BinaryConverter&& other) noexcept = default;
    BinaryConverter& operator=(BinaryConverter const& other) noexcept = default;
    BinaryConverter& operator=(BinaryConverter&& other) noexcept = default;
    ~BinaryConverter() noexcept = default;

    /**
     * @fn convertVector2f
     * @brief this function convert a vector2f into binary and adding the binary signature of vector2f
     * @param pos The sf::Vector2f variable ({x,y})
     *
     * @return The vector of pos byte
     */

    std::vector<Byte> convertVector2f(sf::Vector2f& pos) noexcept;

    /**
     * @fn convertBinaryToVector2f
     * @brief convert binary data to sf::vector2f data.
     * @param data The vector of binary data converted with convertVector2f  method.
     *
     * @return The converted binary to sf::vector2f.
     */
    sf::Vector2f convertBinaryToVector2f(std::vector<Byte>& data) noexcept;

    std::vector<Byte> convertUShortToBinary(unsigned short offset);
    unsigned short convertBinaryToUShort(std::vector<Byte>& rowData) noexcept;

   private:
    /**
     * @fn convertIntToBinary
     * @brief Convert any int data to binary
     *
     * @param data the data you want to convert in binary.
     *
     * @return the int data converted to binary.
     */
    std::vector<Byte> convertIntToBinary(int data);
    /**
     * @var bitset
     * @brief The bitset converter for binary operation
     */
    ecs::core::Bitset bitset;
};
