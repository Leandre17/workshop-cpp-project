/**
 * @file NetworkError.hpp
 * @brief The throw class for network
 * @author Benjamin
 * @version 1.0
 */

#pragma once

#include <exception>
#include <string>

/**
 * @namespace network::error
 * @brief the main namespace for the RType network
 */
namespace network::error {

/**
 * @class NetworkError
 * @brief This class can manage the network Throw
 */
class NetworkError : public std::exception {
   public:
    NetworkError() noexcept = delete;
    /**
     * @fn NetworkError
     * @brief Create the custom message to throw
     * @param message The message to display when throw
     */
    explicit NetworkError(std::string const& message);
    NetworkError(NetworkError const& other) noexcept = delete;
    NetworkError(NetworkError&& other) noexcept = delete;
    NetworkError& operator=(NetworkError const& other) noexcept = delete;
    NetworkError& operator=(NetworkError&& other) noexcept = delete;
    ~NetworkError() noexcept override = default;

    /**
     * @fn what
     * @brief return the message to display.
     *
     * @return the storage message to display
     */
    const char* what() const noexcept final;

   protected:
    /**
     * @var message
     * @brief the message value
     */
    std::string message;
};

} // namespace network::error
