#include "utils/Clock.hpp"

utils::Clock::Clock() noexcept { this->saveTimePoint(); }

void utils::Clock::saveTimePoint() noexcept
{
    this->timepoint = utils::Clock::getActualTime();
}

bool utils::Clock::isElapsed(int milliseconds) const noexcept
{
    time_t const actualTime = getActualTime();

    return (this->timepoint + milliseconds <= actualTime);
}

float utils::Clock::getElapsedTime() const noexcept
{
    time_t const actualTime = getActualTime();

    return actualTime - this->timepoint;
}

time_t utils::Clock::getActualTime() noexcept
{
    struct timeval time_now {
    };

    gettimeofday(&time_now, nullptr);
    return ((time_now.tv_sec * 1000) + (time_now.tv_usec / 1000));
}

