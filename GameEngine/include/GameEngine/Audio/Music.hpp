/**
 * @file Music.hpp
 * @brief Music component
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "SFML/Audio/Music.hpp"

/**
 * @namespace ge::audio
 * @brief Audio related classes
 */
namespace ge::audio {

/**
 * @class Music
 * @brief Music class for ECS
 */
class Music {
   public:
    Music() noexcept = delete;
    /**
     * @brief Construct a music object
     *
     * @param filepath The music file path
     *
     * @throw audio::error::MusicError thrown if the given file is invalid
     */
    explicit Music(std::string filepath);
    Music(Music const& other) noexcept = delete;
    Music(Music&& other) noexcept = delete;
    ~Music() noexcept;

    Music& operator=(Music const& other) noexcept = delete;
    Music& operator=(Music&& other) noexcept = delete;

    /**
     * @brief Play the music
     *
     * @warning If the music have been paused, please use restart if you want to replay the music from start
     */
    void play() noexcept;
    /**
     * @brief Restart the music from start
     *
     * @warning Calling this function will start the music
     */
    void restart() noexcept;
    /**
     * @brief Pause the music
     * @details Use play function to restart the music
     */
    void pause() noexcept;
    /**
     * @brief Stop the music
     */
    void stop() noexcept;
    /**
     * @brief Set the volume of the music
     *
     * @param volume The new volume to apply to the music
     */
    void setVolume(float volume) noexcept;
    /**
     * @brief Setting the loop mode for the music
     *
     * @param looping Set to `true` if the music should automatically restart when get to the end, `false` otherwise
     */
    void setLoop(bool looping = true) noexcept;

   private:
    /**
     * @var music
     * @brief The SFML music object
     */
    sf::Music music;
};

} // namespace ge::audio
