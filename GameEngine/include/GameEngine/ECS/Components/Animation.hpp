/**
 * @file Animation.hpp
 * @brief Animation ofr the entity renderable
 * @author Baptiste-MV
 * @version 1
 */

#pragma once

#include "GameEngine/Utils/Callback.hpp"
#include "GameEngine/Utils/Vector.hpp"
#include "SFML/Graphics/Sprite.hpp"
#include "SFML/System/Time.hpp"
#include "SFML/System/Vector2.hpp"

namespace ge::ecs::component {

/**
 * @class Animation
 * @brief set and make animation
 */
class Animation {
   public:
    Animation() noexcept = delete;
    /**
     * @brief Construct a new Animation object
     *
     * @param frameNumber number of frame in spritesheet
     * @param frameSize area of frame
     * @param frameDuration time between frames
     * @param loop loop or not the animation
     * @param startingFrame the first frame of animation
     */
    Animation(unsigned int frameNumber, utils::Vector frameSize, float frameDuration, bool loop = false, unsigned int startingFrame = 1);
    Animation(Animation const& other) noexcept = delete;
    Animation(Animation&& other) noexcept = default;
    ~Animation() noexcept = default;

    Animation& operator=(Animation const& other) noexcept = delete;
    Animation& operator=(Animation&& other) noexcept = default;
    /**
     * @brief Loop the animation
     *
     * @param looping loop or not the animation
     * @return a referencet to the object itself
     */
    Animation& looping(bool looping = true) noexcept;
    /**
     * @brief reverse the animation
     *
     * @param reverse reverse or not the animation
     * @return a referencet to the object itself
     */
    Animation& reverse(bool reverse = true) noexcept;
    /**
     * @brief Set the Frame Duration object
     *
     * @param newDuration the new duration between frames
     * @return a referencet to the object itself
     */
    Animation& setFrameDuration(float newDuration);
    /**
     * @brief Set the Frame Number object
     *
     * @param newFrameNumber the new number of frames
     * @return a referencet to the object itself
     */
    Animation& setFrameNumber(unsigned int newFrameNumber);
    /**
     * @brief Set the Frame Size object
     *
     * @param newFrameSize the new size of the frame
     * @return a referencet to the object itself
     */
    Animation& setFrameSize(utils::Vector newFrameSize);
    /**
     * @brief set the animation at the beginning
     *
     * @return a referencet to the object itself
     */
    Animation& goToStart() noexcept;
    /**
     * @brief set the animation at the end
     *
     * @return a referencet to the object itself
     */
    Animation& goToEnd() noexcept;
    /**
     * @brief start animation
     *
     * @return a referencet to the object itself
     */
    Animation& start() noexcept;
    /**
     * @brief stop animation
     *
     * @return Animation&
     */
    Animation& stop();
    /**
     * @brief check the next frame
     *
     * @param elapsedTime time of clock
     * @return true
     * @return false
     */
    bool checkNextFrame(float elapsedTime);
    /**
     * @brief Get the New Area object
     *
     * @return rect of the area frame
     */
    sf::IntRect getNewArea() const noexcept;
    /**
     * @brief check if the animation is finished
     *
     * @return true animation finished
     * @return false animation not finished
     */
    bool isEnded() const noexcept;
    template <typename F, typename... Args>
    /**
     * @brief Define the callback that will be called when the animation is finished
     * @pre The function you call must return void and you must give all needed arguments
     *
     * @param f The function pointer to call
     * @param args The arguments to pass to the function pointer
     *
     * @return The animation object itself
     */
    Animation& setCallback(F&& f, Args&&... args) noexcept
    {
        this->callback.define(std::forward<F>(f), std::forward<Args>(args)...);

        return *this;
    }
    /**
     * @brief Define the callback that will be called when the animation is finished
     * @pre The function you call must return void and you must give all needed arguments
     *
     * @param callback The callback that define the function to call
     *
     * @return The animation object itself
     */
    Animation& setCallback(ge::utils::Callback<void()>&& callback) noexcept;
    /**
     * @brief removeCallback define on animation object
     *
     * @return a referencet to the object itself
     */
    Animation& removeCallback() noexcept;

   private:
    /**
     * @var frameNumber
     * @brief number of frames in the animation
     */
    unsigned int frameNumber;
    /**
     * @var currentFrame
     * @brief frame of current animation
     */
    int currentFrame;
    /**
     * @var frameDuration
     * @brief duration between frames in the animation
     */
    unsigned int frameDuration;
    /**
     * @var frameSize
     * @brief size of the frame in the animation
     */
    utils::Vector frameSize;
    /**
     * @var callback function
     * @brief
     */
    ge::utils::Callback<void()> callback;
    /**
     * @var loop
     * @brief if animation is looping
     */
    bool loop = false;
    /**
     * @var reversing
     * @brief if the animation is reversing
     */
    bool reversing = false;
    /**
     * @var running
     * @brief if the animation is running
     */
    bool running = false;
    /**
     * @var upToDate
     * @brief if the animation is up to date
     */
    bool upToDate = true;
    /**
     * @var currentFrameDuration
     * @brief the acutal duration of frame in seconds
     */
    float currentFrameDuration = 0;
};

} // namespace ge::ecs::component