/**
 * @file Vector.hpp
 * @brief Vector object
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "SFML/System/Vector2.hpp"

#include <functional>

namespace ge::utils {

/**
 * @class Vector
 * @brief Bidimensional vector
 */
class Vector {
   public:
    constexpr Vector() noexcept = default;
    template <typename T, typename U>
    constexpr Vector(T x, U y) noexcept :
        x(x), y(y)
    {
    }
    constexpr Vector(Vector const& other) noexcept = default;
    constexpr Vector(Vector&& other) noexcept = default;
    constexpr ~Vector() noexcept = default;

    Vector& operator=(Vector const& other) noexcept = default;
    Vector& operator=(Vector&& other) noexcept = default;

    constexpr Vector operator+(Vector other) const noexcept
    {
        return {this->x + other.x, this->y + other.y};
    }
    constexpr Vector operator+(float other) const noexcept
    {
        return {this->x + other, this->y + other};
    }
    constexpr Vector operator-(Vector other) const noexcept
    {
        return {this->x - other.x, this->y - other.y};
    }
    constexpr Vector operator-(float other) const noexcept
    {
        return {this->x - other, this->y - other};
    }
    constexpr Vector operator*(Vector other) const noexcept
    {
        return {this->x * other.x, this->y * other.y};
    }
    constexpr Vector operator*(float other) const noexcept
    {
        return {this->x * other, this->y * other};
    }
    constexpr Vector operator/(Vector other) const noexcept
    {
        if (other.x == 0 or other.y == 0) {
            return *this;
        }
        return {this->x / other.x, this->y / other.y};
    }
    constexpr Vector operator/(float other) const noexcept
    {
        if (other == 0) {
            return *this;
        }
        return {this->x / other, this->y / other};
    }
    constexpr Vector& operator++() noexcept
    {
        this->x++;
        this->y++;

        return *this;
    }
    constexpr Vector& operator--() noexcept
    {
        this->x--;
        this->y--;

        return *this;
    }
    constexpr bool operator==(Vector const& other) const noexcept
    {
        return (this->x == other.x and this->y == other.y);
    }
    constexpr bool operator!=(Vector const& other) const noexcept
    {
        return (this->x != other.x or this->y != other.y);
    }
    constexpr bool operator>(Vector const& other) const noexcept
    {
        return (this->x > other.x and this->y > other.y);
    }
    constexpr bool operator<(Vector const& other) const noexcept
    {
        return (this->x < other.x and this->y < other.y);
    }
    constexpr bool operator>=(Vector const& other) const noexcept
    {
        return (this->x >= other.x and this->y >= other.y);
    }
    constexpr bool operator<=(Vector const& other) const noexcept
    {
        return (this->x <= other.x and this->y <= other.y);
    }
    constexpr bool operator!() const noexcept
    {
        return (this->x == 0 and this->y == 0);
    }
    constexpr Vector& operator+=(Vector other) noexcept
    {
        this->x += other.x;
        this->y += other.y;

        return *this;
    }
    constexpr Vector& operator-=(Vector other) noexcept
    {
        this->x -= other.x;
        this->y -= other.y;

        return *this;
    }
    constexpr Vector& operator*=(Vector other) noexcept
    {
        this->x *= other.x;
        this->y *= other.y;

        return *this;
    }
    constexpr Vector& operator/=(Vector other) noexcept
    {
        if (other.x == 0 or other.y == 0) {
            return *this;
        }
        this->x /= other.x;
        this->y /= other.y;

        return *this;
    }

    // NOLINTNEXTLINE
    operator sf::Vector2f() const;
    // NOLINTNEXTLINE
    operator sf::Vector2i() const;

    /**
     * @var x
     * @brief The position on the x axis
     */
    float x = 0;
    /**
     * @var y
     * @brief The position on the y axis
     */
    float y = 0;
};

/**
 * @struct VectorHash
 * @brief The Vector object hasher
 */
struct VectorHash {
    std::size_t operator()(const Vector& vector) const noexcept;
};

} // namespace ge::utils
