#include "GameEngine/ECS/Core/EntitiesManager.hpp"

#include "GameEngine/ECS/ECSErrors.hpp"
#include "fmt/core.h"

#include <algorithm>
#include <iostream>

ge::ecs::core::Entity ge::ecs::core::EntitiesManager::createEntity() noexcept
{
    if (not this->availableEntities.empty()) {
        auto got = this->availableEntities.back();
        this->availableEntities.pop_back();
        return Entity{got};
    }
    if (not this->used) {
        this->used = true;
    } else {
        this->maxExistingEntity++;
    }
    if (this->entitiesSignatures.size() != this->maxExistingEntity + 1) {
        this->entitiesSignatures.resize(this->maxExistingEntity + 1);
    }
    return Entity{this->maxExistingEntity};
}

void ge::ecs::core::EntitiesManager::destroyEntity(Entity const& entity) noexcept
{
    if (std::find(this->availableEntities.begin(), this->availableEntities.end(), entity) != this->availableEntities.end() or entity > this->maxExistingEntity) {
        std::cout << fmt::format("WARNING: Deleting the same entity twice ({})", entity) << std::endl;
    } else {
        this->entitiesSignatures[entity].clear();
        this->availableEntities.emplace_back(entity);
    }
}

void ge::ecs::core::EntitiesManager::setSignature(Entity const& entity, ComponentSignature signature)
{
    if (std::find(this->availableEntities.begin(), this->availableEntities.end(), entity) != this->availableEntities.end() or entity > this->maxExistingEntity) {
        throw ge::ecs::error::ECSError("Setting signature of non existing or destroyed entity");
    }
    this->entitiesSignatures[entity] = std::forward<ComponentSignature>(signature);
}

void ge::ecs::core::EntitiesManager::sign(Entity const& entity, utils::Bit componentBit, bool value)
{
    if (std::find(this->availableEntities.begin(), this->availableEntities.end(), entity) != this->availableEntities.end() or entity > this->maxExistingEntity) {
        throw ge::ecs::error::ECSError("Changing signature of non existing or destroyed entity");
    }
    this->entitiesSignatures[entity].set(componentBit, value);
}

bool ge::ecs::core::EntitiesManager::entityExist(std::size_t id) const noexcept
{
    return (this->used and (id <= this->maxExistingEntity and std::find(this->availableEntities.begin(), this->availableEntities.end(), id) == this->availableEntities.end()));
}

std::size_t ge::ecs::core::EntitiesManager::getMaxExistingEntity() const noexcept
{
    return this->maxExistingEntity;
}

ge::ecs::core::ComponentSignature ge::ecs::core::EntitiesManager::getSignature(Entity const& entity) const
{
    if (std::find(this->availableEntities.begin(), this->availableEntities.end(), entity) != this->availableEntities.end() or entity > this->maxExistingEntity) {
        throw ge::ecs::error::ECSError("Getting signature of non existing or destroyed entity");
    }
    return this->entitiesSignatures.at(entity);
}
