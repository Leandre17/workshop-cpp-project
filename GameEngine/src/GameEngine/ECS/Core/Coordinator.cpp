#include "GameEngine/ECS/Core/Coordinator.hpp"

ge::ecs::core::Entity ge::ecs::core::Coordinator::createEntity() noexcept
{
    return this->entitiesManager.createEntity();
}

void ge::ecs::core::Coordinator::destroyEntity(Entity const& entity) noexcept
{
    this->systemsManager.entityDestroyed(entity);
    this->entitiesManager.destroyEntity(entity);
}
