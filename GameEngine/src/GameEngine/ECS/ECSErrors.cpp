#include "GameEngine/ECS/ECSErrors.hpp"

ge::ecs::error::ECSError::ECSError(std::string const& message) :
    message(message) { }

const char* ge::ecs::error::ECSError::what() const noexcept
{
    return this->message.c_str();
}

ge::ecs::error::ComponentError::ComponentError(std::string const& name, std::string const& message) :
    ECSError(message), name(name) { }

const char* ge::ecs::error::ComponentError::which() const noexcept
{
    return this->name.c_str();
}
