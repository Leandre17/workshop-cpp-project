#if defined USE_TEXT_MODULE && defined USE_COLLISION_MODULE

#include "GameEngine/ECS/Systems/TextBoundsSystem.hpp"

ge::ecs::system::TextBoundsSystem::TextBoundsSystem(ecs::core::Coordinator& coordinator) noexcept :
    ge::ecs::core::ASystem(coordinator) { }

void ge::ecs::system::TextBoundsSystem::update()
{
    for (auto const& entity : this->entities) {
        auto& text = this->coordinator.getComponent<ecs::component::RenderableText>(entity);
        auto& bounds = this->coordinator.getComponent<ecs::component::Bounds>(entity);

        bounds.bounds = text.getBounds();
    }
}

#endif