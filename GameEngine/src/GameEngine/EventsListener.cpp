#include "GameEngine/EventsListener.hpp"

#include "SFML/Window/Event.hpp"

// NOLINTNEXTLINE
std::vector<ge::Event> ge::EventsListener::getEvents(sf::RenderWindow& window) const noexcept
{
    static const std::unordered_map<sf::Keyboard::Key, ge::KeyboardKeys> sfmlKeys({
        {sf::Keyboard::Key::A, ge::KeyboardKeys::A},
        {sf::Keyboard::Key::B, ge::KeyboardKeys::B},
        {sf::Keyboard::Key::C, ge::KeyboardKeys::C},
        {sf::Keyboard::Key::D, ge::KeyboardKeys::D},
        {sf::Keyboard::Key::E, ge::KeyboardKeys::E},
        {sf::Keyboard::Key::F, ge::KeyboardKeys::F},
        {sf::Keyboard::Key::G, ge::KeyboardKeys::G},
        {sf::Keyboard::Key::H, ge::KeyboardKeys::H},
        {sf::Keyboard::Key::I, ge::KeyboardKeys::I},
        {sf::Keyboard::Key::J, ge::KeyboardKeys::J},
        {sf::Keyboard::Key::K, ge::KeyboardKeys::K},
        {sf::Keyboard::Key::L, ge::KeyboardKeys::L},
        {sf::Keyboard::Key::M, ge::KeyboardKeys::M},
        {sf::Keyboard::Key::N, ge::KeyboardKeys::N},
        {sf::Keyboard::Key::O, ge::KeyboardKeys::O},
        {sf::Keyboard::Key::P, ge::KeyboardKeys::P},
        {sf::Keyboard::Key::Q, ge::KeyboardKeys::Q},
        {sf::Keyboard::Key::R, ge::KeyboardKeys::R},
        {sf::Keyboard::Key::S, ge::KeyboardKeys::S},
        {sf::Keyboard::Key::T, ge::KeyboardKeys::T},
        {sf::Keyboard::Key::U, ge::KeyboardKeys::U},
        {sf::Keyboard::Key::V, ge::KeyboardKeys::V},
        {sf::Keyboard::Key::W, ge::KeyboardKeys::W},
        {sf::Keyboard::Key::X, ge::KeyboardKeys::X},
        {sf::Keyboard::Key::Y, ge::KeyboardKeys::Y},
        {sf::Keyboard::Key::Z, ge::KeyboardKeys::Z},
        {sf::Keyboard::Key::Num0, ge::KeyboardKeys::Num0},
        {sf::Keyboard::Key::Num1, ge::KeyboardKeys::Num1},
        {sf::Keyboard::Key::Num2, ge::KeyboardKeys::Num2},
        {sf::Keyboard::Key::Num3, ge::KeyboardKeys::Num3},
        {sf::Keyboard::Key::Num4, ge::KeyboardKeys::Num4},
        {sf::Keyboard::Key::Num5, ge::KeyboardKeys::Num5},
        {sf::Keyboard::Key::Num6, ge::KeyboardKeys::Num6},
        {sf::Keyboard::Key::Num7, ge::KeyboardKeys::Num7},
        {sf::Keyboard::Key::Num8, ge::KeyboardKeys::Num8},
        {sf::Keyboard::Key::Num9, ge::KeyboardKeys::Num9},
        {sf::Keyboard::Key::Escape, ge::KeyboardKeys::Escape},
        {sf::Keyboard::Key::LControl, ge::KeyboardKeys::LControl},
        {sf::Keyboard::Key::LShift, ge::KeyboardKeys::LShift},
        {sf::Keyboard::Key::LAlt, ge::KeyboardKeys::LAlt},
        {sf::Keyboard::Key::LSystem, ge::KeyboardKeys::LSystem},
        {sf::Keyboard::Key::RControl, ge::KeyboardKeys::RControl},
        {sf::Keyboard::Key::RShift, ge::KeyboardKeys::RShift},
        {sf::Keyboard::Key::RAlt, ge::KeyboardKeys::RAlt},
        {sf::Keyboard::Key::RSystem, ge::KeyboardKeys::RSystem},
        {sf::Keyboard::Key::Menu, ge::KeyboardKeys::Menu},
        {sf::Keyboard::Key::LBracket, ge::KeyboardKeys::LBracket},
        {sf::Keyboard::Key::RBracket, ge::KeyboardKeys::RBracket},
        {sf::Keyboard::Key::Semicolon, ge::KeyboardKeys::Semicolon},
        {sf::Keyboard::Key::Comma, ge::KeyboardKeys::Comma},
        {sf::Keyboard::Key::Period, ge::KeyboardKeys::Period},
        {sf::Keyboard::Key::Quote, ge::KeyboardKeys::Quote},
        {sf::Keyboard::Key::Slash, ge::KeyboardKeys::Slash},
        {sf::Keyboard::Key::Backslash, ge::KeyboardKeys::Backslash},
        {sf::Keyboard::Key::Tilde, ge::KeyboardKeys::Tilde},
        {sf::Keyboard::Key::Equal, ge::KeyboardKeys::Equal},
        {sf::Keyboard::Key::Hyphen, ge::KeyboardKeys::Hyphen},
        {sf::Keyboard::Key::Space, ge::KeyboardKeys::Space},
        {sf::Keyboard::Key::Enter, ge::KeyboardKeys::Enter},
        {sf::Keyboard::Key::Backspace, ge::KeyboardKeys::Backspace},
        {sf::Keyboard::Key::Tab, ge::KeyboardKeys::Tab},
        {sf::Keyboard::Key::PageUp, ge::KeyboardKeys::PageUp},
        {sf::Keyboard::Key::PageDown, ge::KeyboardKeys::PageDown},
        {sf::Keyboard::Key::End, ge::KeyboardKeys::End},
        {sf::Keyboard::Key::Home, ge::KeyboardKeys::Home},
        {sf::Keyboard::Key::Insert, ge::KeyboardKeys::Insert},
        {sf::Keyboard::Key::Delete, ge::KeyboardKeys::Delete},
        {sf::Keyboard::Key::Add, ge::KeyboardKeys::Add},
        {sf::Keyboard::Key::Subtract, ge::KeyboardKeys::Subtract},
        {sf::Keyboard::Key::Multiply, ge::KeyboardKeys::Multiply},
        {sf::Keyboard::Key::Divide, ge::KeyboardKeys::Divide},
        {sf::Keyboard::Key::Left, ge::KeyboardKeys::Left},
        {sf::Keyboard::Key::Right, ge::KeyboardKeys::Right},
        {sf::Keyboard::Key::Up, ge::KeyboardKeys::Up},
        {sf::Keyboard::Key::Down, ge::KeyboardKeys::Down},
        {sf::Keyboard::Key::Numpad0, ge::KeyboardKeys::Numpad0},
        {sf::Keyboard::Key::Numpad1, ge::KeyboardKeys::Numpad1},
        {sf::Keyboard::Key::Numpad2, ge::KeyboardKeys::Numpad2},
        {sf::Keyboard::Key::Numpad3, ge::KeyboardKeys::Numpad3},
        {sf::Keyboard::Key::Numpad4, ge::KeyboardKeys::Numpad4},
        {sf::Keyboard::Key::Numpad5, ge::KeyboardKeys::Numpad5},
        {sf::Keyboard::Key::Numpad6, ge::KeyboardKeys::Numpad6},
        {sf::Keyboard::Key::Numpad7, ge::KeyboardKeys::Numpad7},
        {sf::Keyboard::Key::Numpad8, ge::KeyboardKeys::Numpad8},
        {sf::Keyboard::Key::Numpad9, ge::KeyboardKeys::Numpad9},
        {sf::Keyboard::Key::F1, ge::KeyboardKeys::F1},
        {sf::Keyboard::Key::F2, ge::KeyboardKeys::F2},
        {sf::Keyboard::Key::F3, ge::KeyboardKeys::F3},
        {sf::Keyboard::Key::F4, ge::KeyboardKeys::F4},
        {sf::Keyboard::Key::F5, ge::KeyboardKeys::F5},
        {sf::Keyboard::Key::F6, ge::KeyboardKeys::F6},
        {sf::Keyboard::Key::F7, ge::KeyboardKeys::F7},
        {sf::Keyboard::Key::F8, ge::KeyboardKeys::F8},
        {sf::Keyboard::Key::F9, ge::KeyboardKeys::F9},
        {sf::Keyboard::Key::F10, ge::KeyboardKeys::F10},
        {sf::Keyboard::Key::F11, ge::KeyboardKeys::F11},
        {sf::Keyboard::Key::F12, ge::KeyboardKeys::F12},
        {sf::Keyboard::Key::F13, ge::KeyboardKeys::F13},
        {sf::Keyboard::Key::F14, ge::KeyboardKeys::F14},
        {sf::Keyboard::Key::F15, ge::KeyboardKeys::F15},
        {sf::Keyboard::Key::Pause, ge::KeyboardKeys::Pause},
    });

    sf::Event e{};
    std::vector<Event> events;

    while (window.pollEvent(e)) {
        switch (e.type) {
        case sf::Event::Closed:
            window.close();
            events.emplace_back(ge::EventsKeys::Closed);
            break;
        case sf::Event::Resized:
            events.emplace_back(ge::EventsKeys::Resized);
            break;
        case sf::Event::LostFocus:
            events.emplace_back(ge::EventsKeys::LostFocus);
            break;
        case sf::Event::GainedFocus:
            events.emplace_back(ge::EventsKeys::GainedFocus);
            break;
        case sf::Event::KeyPressed:
            if (sfmlKeys.contains(e.key.code)) {
                events.emplace_back(ge::EventsKeys::KeyboardPressed, sfmlKeys.at(e.key.code));
            }
            break;
        case sf::Event::KeyReleased:
            if (sfmlKeys.contains(e.key.code)) {
                events.emplace_back(ge::EventsKeys::KeyboardReleased, sfmlKeys.at(e.key.code));
            }
            break;
        default:
            break;
        }
    }

    return events;
}
