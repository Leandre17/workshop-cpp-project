#include "GameEngine/Utils/Utils.hpp"

#include <cmath>
#include <numbers>

float ge::utils::getRotationAngleFromX(const utils::Vector& vector) noexcept
{
    return std::atan2(vector.y, vector.x) * (180 / std::numbers::pi);
}
