#ifdef USE_AUDIO_MODULE

#include "GameEngine/Audio/SoundPlayer.hpp"

#include "GameEngine/ResourcesManager.hpp"
#include "fmt/core.h"

ge::audio::Sound::Sound(const std::string& filePath, float volume)
{
    auto& buffer = ge::ResourcesManager::getSoundBuffer(filePath);

    this->sound.setBuffer(buffer);
    this->sound.setVolume(volume);
    this->sound.play();
}

void ge::audio::Sound::setVolume(float volume) noexcept
{
    this->sound.setVolume(volume);
}

bool ge::audio::Sound::isEnded() const noexcept
{
    return (this->sound.getStatus() == sf::Sound::Stopped);
}

void ge::audio::SoundPlayer::deleteEndedSounds() noexcept
{
    auto& instance = SoundPlayer::getInstance();

    for (auto it = instance.sounds.begin(); it != instance.sounds.end(); it++) {
        if (it->isEnded()) {
            instance.sounds.erase(it);
            return SoundPlayer::deleteEndedSounds();
        }
    }
}

void ge::audio::SoundPlayer::playSound(const std::string& filePath)
{
    SoundPlayer::deleteEndedSounds();

    auto& instance = SoundPlayer::getInstance();

    float volume = instance.volume;
    if (instance.specificVolumes.contains(filePath)) {
        volume = instance.specificVolumes.at(filePath);
    }

    instance.sounds.emplace_back(filePath, volume);
}

void ge::audio::SoundPlayer::setSoundVolume(std::string filePath, float volume) noexcept
{
    auto& instance = SoundPlayer::getInstance();

    if (instance.specificVolumes.contains(filePath)) {
        instance.specificVolumes.at(filePath) = volume;
    } else {
        instance.specificVolumes.insert({std::move(filePath), volume});
    }
}

void ge::audio::SoundPlayer::setSoundsVolume(float volume) noexcept
{
    auto& instance = SoundPlayer::getInstance();

    instance.volume = volume;
    for (auto& sound : instance.sounds) {
        sound.setVolume(volume);
    }
}

#endif
