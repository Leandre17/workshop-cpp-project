#include "GameEngine/ResourcesManager.hpp"

#include "GameEngine/Utils/FilePaths.hpp"
#include "fmt/core.h"

ge::error::ResourceError::ResourceError(std::string const& message) :
    message(message)
{
}

const char* ge::error::ResourceError::what() const noexcept
{
    return this->message.c_str();
}

sf::Font& ge::ResourcesManager::getFont(std::string filepath)
{
    filepath = fmt::format("{}/fonts/{}", ASSETSPATH, filepath);
    auto& instance = getInstance();

    if (not instance.fonts.contains(filepath)) {
        sf::Font font;
        if (not font.loadFromFile(filepath)) {
            throw error::ResourceError(fmt::format("Cannot use font file: {}", filepath));
        }
        instance.fonts.emplace(filepath, font);
    }

    return instance.fonts.at(filepath);
}

sf::Texture& ge::ResourcesManager::getTexture(std::string filepath)
{
    filepath = fmt::format("{}/images/{}", ASSETSPATH, filepath);
    auto& instance = getInstance();

    if (not instance.textures.contains(filepath)) {
        sf::Texture texture;
        if (not texture.loadFromFile(filepath)) {
            throw error::ResourceError(fmt::format("Cannot use texture file: {}", filepath));
        }
        instance.textures.emplace(filepath, texture);
    }

    return instance.textures.at(filepath);
}

sf::SoundBuffer& ge::ResourcesManager::getSoundBuffer(std::string filepath)
{
    filepath = fmt::format("{}/sounds/{}", ASSETSPATH, filepath);
    auto& instance = getInstance();

    if (not instance.sounds.contains(filepath)) {
        sf::SoundBuffer buffer;
        if (not buffer.loadFromFile(filepath)) {
            throw error::ResourceError(fmt::format("Cannot use sound file: {}", filepath));
        }
        instance.sounds.emplace(filepath, buffer);
    }

    return instance.sounds.at(filepath);
}
