#include "GameEngine/Window.hpp"

#include "SFML/Window/VideoMode.hpp"

ge::Window::Window(const std::string& title) noexcept :
    _inited(true), window(sf::VideoMode::getDesktopMode(), sf::String(title))
{
    this->window.setKeyRepeatEnabled(false);
}

ge::Window::Window(unsigned int width, unsigned int height, const std::string& title, unsigned int bitsPerPixel) noexcept :
    _inited(true), window(sf::VideoMode({width, height}, bitsPerPixel), sf::String(title))
{
    this->window.setKeyRepeatEnabled(false);
}

void ge::Window::init(const std::string& title) noexcept
{
    this->window.create(sf::VideoMode::getDesktopMode(), sf::String(title));
    this->window.setKeyRepeatEnabled(false);
}

void ge::Window::init(unsigned int width, unsigned int height, const std::string& title, unsigned int bitsPerPixel) noexcept
{
    this->window.create(sf::VideoMode({width, height}, bitsPerPixel), sf::String(title));
    this->window.setKeyRepeatEnabled(false);
}

void ge::Window::close() noexcept
{
    this->window.close();
}

bool ge::Window::isOpen() const noexcept
{
    return this->window.isOpen();
}

void ge::Window::clear() noexcept
{
    this->window.clear();
}

void ge::Window::display() noexcept
{
    this->window.display();
}

std::vector<ge::Event> ge::Window::getEvents() noexcept
{
    return this->eventsListener.getEvents(this->window);
}

ge::utils::Vector ge::Window::getSize() const noexcept
{
    const auto& size = this->window.getSize();
    return {size.x, size.y};
}
